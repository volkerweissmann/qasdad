#!/usr/bin/python3
#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

print("qasdadp.py started")

import os
import shutil
import sys
import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("source", help="Path of the source dirctory.")
parser.add_argument("--texonly", action="store_true", help="You may use this flag if you only changed the latex parts and not the python parts.")
parser.add_argument("--dontrun", action="store_true", help="We will not run the python.py script.")
#parser.add_argument("--onlysec", help="--onlySec=2,5 would make qasdad ignore all \\py sections with a number that is not equal to 2 or 5")
parser.add_argument("--notexplot", action="store_true", help="Disables the use of tex in rendering of plots. Use this if your tex installation is faulty.")
args = parser.parse_args()

sourcePath = args.source
if not os.path.exists(sourcePath):
	print("source does not exist: ", sourcePath)
	exit(2)
if not os.path.isfile(sourcePath) and not os.path.isdir(sourcePath):
	print("source is neither a file nor a directory: ", sourcePath)
	exit(2)

#simpath("abc") returns sourcePath_abc
def simpath(suffix):
	sps = list(os.path.split(sourcePath))
	if sps[-1] == "": #This if is neccessary because the user can run qasdad.py dirA/ dirB or qasdad.py dirA dirB
		sps = sps[:-1]
	sps[-1] += suffix
	dpath = os.path.join(*sps)
	return dpath

cachePath = simpath("_qasdad_cache")
try:
	os.makedirs(cachePath)
except FileExistsError:
	pass
outPath = simpath("_qasdad_output")
dataDir = "qasdad_out"
dataPath = os.path.join(outPath, dataDir)
pythonPath = os.path.join(dataPath, "python.py")

if args.texonly:
	for name in os.listdir(outPath):
		if name != dataDir:
			path = os.path.join(outPath,name)
			if os.path.isdir(path):
				shutil.rmtree(path)
			elif os.path.isfile(path):
				os.remove(path)
			else:
				print("qasdadp.py is confused, because the following is neither a file nor a directory:", path)
				exit(2)
	try:
		os.remove(pythonPath)
	except FileNotFoundError:
		pass
else:
	try:
		shutil.rmtree(outPath)
	except FileNotFoundError:
		pass
	os.makedirs(dataPath)


def pyDoubleQuoteEscape(string):
	escaped = string.replace("\\","\\\\").replace("\"","\\\"")
	assert(string == eval("\""+escaped+"\""))
	return escaped

extracted = {}

def readFile(sourcepath, latexout):
	source = open(sourcepath, "r").read() #TODO Is it necessary to explicitly close this file
	try:
		os.makedirs(os.path.dirname(latexout))
	except FileExistsError:
		pass
	latexFile = open(latexout, "w") #TODO Is it necessary to explicitly close this file
	num = None
	i = 0
	while i < len(source):
		if source[i:i+len("\\py(")] == "\\py(":
			if num is not None:
				print("File: " + sourcepath + "\nLine:" + str(1+source[:i].count("\n"))+"\nYou cannot have a \\py after another \\py without a \\tex in between.")
				exit(3)
			i += len("\\py(")
			start = i
			while not source[i] == ")":
				i += 1
			try:
				num = float(source[start:i])
			except:
				print("File: " + sourcepath + "\nLine:" + str(1+source[:i].count("\n"))+"\nCorrect syntax: \\py(float)")
				exit(3)
			i += 1
			while source[i].isspace():
				i += 1
			if num in extracted:
				print("File: " + sourcepath + "\nLine:" + str(1+source[:i].count("\n"))+"\nmultiple sections with the number" + str(num))
				exit(3)
			gentexPath = os.path.join(dataPath,str(num) + ".tex")
			#In the line below, we assume, that start=outPath is the working directory of python.py
			#extracted[num] = [sourcepath,1+source[:i].count("\n"),"\nsetTexFile(open(\""+ pyDoubleQuoteEscape(os.path.relpath(gentexPath, start=outPath)) +"\",'w'))\n"]
			extracted[num] = [sourcepath,1+source[:i].count("\n"),"\nsetTexFile(open(\""+ pyDoubleQuoteEscape(os.path.relpath(gentexPath, start=outPath)) +"\",'w', encoding='utf-8'))\n"]
			rawpath = os.path.relpath(gentexPath,start=os.path.dirname(latexout))
			if os.name == "nt":
				latexFile.write("\\input{" + rawpath.replace("\\","/") + "}") #TODO it is better to use latex relative path import here. #TODO check what special characters (\ and #) there are in LaTex
			else:
				assert("\\" not in rawpath)
				latexFile.write("\\input{" + rawpath + "}") #TODO it is better to use latex relative path import here. #TODO check what special characters (\ and #) there are in LaTex
			continue
		elif source[i:i+4] == "\\tex":
			if i+4<len(source) and not source[i+4] in [" ", "\t", "\n"]:
				pass
				#print("File: " + sourcepath + "\nLine:" + str(1+source[:i].count("\n")) +"\nWarning:You need a whitespace after \\tex")
			else:
				if num is None:
					print("File: " + sourcepath + "\nLine:" + str(1+source[:i].count("\n")) +"\n\\tex without \\py")
					exit(3)
				num = None
				i += len("\\tex")
				continue
		if num is None:
			latexFile.write(source[i])
		else:
			extracted[num][2] += source[i]
			#if args.onlysec is None:
			#	extracted[num][2] += source[i]
			#else:
			#	seclist = [float(s) for s in args.onlysec.split(",")]
			#	if num in seclist:
			#		extracted[num][2] += source[i]
		i += 1

def relsymlink(origpath, linkpath, dirorfile):
	if os.name == "nt":
		if dirorfile == "dir":
			#os.system('mklink /J "' + linkpath + '" "' + origpath + '"')
			shutil.copytree(origpath, linkpath)
		elif dirorfile == "file":
			#os.system('mklink /H "' + linkpath + '" "' + origpath + '"')
			os.link(origpath, linkpath)
		else:
			raise ValueError("dirorfile is neither dir nor file")
	else:
		os.symlink(os.path.relpath(origpath,start=os.path.dirname(linkpath)), linkpath)
	
if os.path.isfile(sourcePath):
	readFile(sourcePath, os.path.join(outPath,os.path.basename(sourcePath)))
elif os.path.isdir(sourcePath):
	for subdir, dirs, files in os.walk(sourcePath):
		for file in files:
			sourcepath = os.path.join(subdir, file)
			rel = os.path.relpath(sourcepath,start=sourcePath)
			pathout = os.path.join(outPath,rel)
			if(file.endswith(".tex")):
				readFile(sourcepath,pathout)
			else:
				os.makedirs(os.path.dirname(pathout), exist_ok=True)
				relsymlink(sourcepath, pathout, "file")
				#shutil.copy(sourcepath,pathout)
else:
	print("the source argument:", sourcePath, "is neither a file nor a directory") #This case should never trigger cause we check this above
	exit(2)
if args.texonly:
	exit(0)

relsymlink(os.path.join(*os.path.split(sys.argv[0])[:-1], "src"), os.path.join(dataPath, "qasdad"), "dir")
relsymlink(os.path.join(*os.path.split(sys.argv[0])[:-1], "..", "latex2sympy", "latex2sympy.py"), os.path.join(dataPath, "latex2sympy.py"), "file")

pythonFile = open(pythonPath, "w")
pythonFile.write("from qasdad import *\n")
pythonFile.write("print('pythonfile started')\n"
		 "setDataPath(\"" + os.path.relpath(dataPath, start=outPath).replace("\"","\\\"")  + "\")\n"
		 "setCachePath(\"" + os.path.relpath(cachePath, start=outPath).replace("\"","\\\"")  + "\")\n"
		 "setLatexPath(\"" + os.path.relpath(outPath, start=outPath).replace("\\","\\\\").replace("\"","\\\"")  + "\")\n" #setLatexPath is the working directory of pdflatex file relative from the working directory of python.py. So its always (at least for now) equal to "."
		 "setNoTexPlot("+ str(args.notexplot) +")\n") #Todo muss man nicht zwei escapes machen, einen für ", einen für \?
lineMap = []
linecount = 8
for key in sorted(extracted.keys()):
	lines = extracted[key][2].count("\n")
	lineMap.append((extracted[key][0], extracted[key][1], linecount, lines))
	pythonFile.write(extracted[key][2])
	linecount += lines
pythonFile.close()
if args.dontrun:
	exit(0)

def mapLineForExceptions(filename, line):
	if filename == "qasdad_out/python.py":
		for ar in lineMap:
			if line >= ar[2] and line < ar[2]+ar[3]:
				return ar[0], line + ar[1] - ar[2]
		return filename,line #This should never be called
	else:
		return filename, line
os.chdir(outPath)
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", "qasdad_out/python.py")
foo = importlib.util.module_from_spec(spec)
sys.path.insert(0, "qasdad_out")
import traceback
try:
	spec.loader.exec_module(foo)
except Exception as e:
	exc_type, exc_obj, exc_tb = sys.exc_info()
	counter = 0
	for frame in traceback.extract_tb(exc_tb):
		counter += 1
		if counter < 4:#Todo ist das definitionsgemäß gleich 4? Was ist wenn spec.loader.exec_module geändert wird
			continue
		row = []
		filename, lineno = mapLineForExceptions(frame.filename,frame.lineno)
		row.append('  File "{}", line {}, in {}\n'.format(
		filename, lineno, frame.name))
		if frame.line:
			row.append('    {}\n'.format(frame.line.strip()))
		if frame.locals:
			for name, value in sorted(frame.locals.items()):
				row.append('    {name} = {value}\n'.format(name=name, value=value))
		print(''.join(row),end="")
	exceptMsg = "".join(traceback.format_exception_only(exc_type, exc_obj))
	posString = "  File \"qasdad_out/python.py\", line "
	if exceptMsg.startswith(posString):
		lineno = int(exceptMsg[len(posString):].split("\n")[0])
		filename, lineno = mapLineForExceptions("qasdad_out/python.py",lineno)
		print('  File "{}", line {}'.format(
		filename, lineno))
		print("\n".join(exceptMsg[len(posString):].split("\n")[1:]), end="")
	else:
		print(exceptMsg, end="")
	exit(1)