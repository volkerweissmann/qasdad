#!/usr/bin/env python3

import unittest
from src import *


class MyTests(unittest.TestCase):
	def assertFloatEqual(self, a, b):
		if abs(a-b) > 0.001:
			raise AssertionError(str(a)+" is not "+str(b))

	#Special 1: If you call Equation.fromSympy with an equation that includes pi, you would want Equation.eqLatex to have a \\pi in it and not a 3.14. This depends on anythingExceptNumbersToTex, this depends on roundSympy
	def testSpecial1(self):
		self.assertEqual(roundSympy(sympy.pi), sympy.pi)
		self.assertEqual(roundSympy(sympy.pi*symbols("a")), sympy.pi*symbols("a"))

		self.assertEqual(anythingExceptNumbersToTex("pi"), "\\pi")
		self.assertEqual(anythingExceptNumbersToTex("pi*a"), "\\pi a")

		eq = Equation.fromSympy("a=pi", {})
		self.assertEqual(eq.eqLatex, "a = \\pi")

	#Special 2: Equation.fromSympy("a=dt", {"\\Delta t": "dt"}) should not crash because sympy.symbols("\\Delta t") is problematic because it has a space in it.
	def testSpecial2(self):
		eq = Equation.fromSympy("a=dt", {"\\Delta t": "dt"})
		self.assertEqual(eq.eqLatex, "a = \\Delta{}t")

	#Special 3: Equation.fromSympy("a=atan(b)") should result in Equation.eqLatex == "a = \\arctan{\\left(b \\right)}a = \arctan" and not "a = \\operatorname{atan}{\\left(b \\right)}"
	def testSpecial3(self):
		eq = Equation.fromSympy("a=atan(b)")
		self.assertEqual(eq.eqLatex, "a = \\arctan{\\left(b \\right)}")

	#Special 4: calcColumn replaces the latex name of variables by the values of the variables in the example Calculation. This can be problematic when the latex name of the variables has a space in it, because "\\Delta{}a".replace("\\Delta a", "17") does not work.
	#b = 2 \\Delta{}a\\\\= 2 \\Delta{}a\\\\=\\ensuremath{34{,}0}\\ensuremath{}
	def testSpecial4(self):
		da = Column(Name("da", eqName="\\Delta a"), 1, [17])
		b = calcColumn([da], "b=2*da")
		#b.exampleEquation should not be b = 2 \\Delta{}a\\\\= 2 \\Delta{}a\\\\=\\ensuremath{34{,}0}\\ensuremath{}  , but the string below
		self.assertEqual(b.exampleEquation, r"b = 2 \Delta{}a\\= 2 \left(\ensuremath{17}\ensuremath{{.}0}\ensuremath{}\right)\\=\ensuremath{34}\ensuremath{{.}0}\ensuremath{}")
		
	#Special 5: sympy.latex(1/(ab)) returns 1 / a b if fold_short_frac=True
	def testSpecial5(self):
		symfunc = 1/(symbols("a")*symbols("b"))
		latfunc = anythingExceptNumbersToTex(symfunc)
		# latfunc should not be 1 / a b
		self.assertEqual(latfunc, r"1 / \left(a b\right)")

	#Special 6: calcColumn([a], "a**2") needs to set the eqName to "a^{2}"
	def testSpecial6(self):
		a = Column(Name("a"), 1, [17])
		
		b = calcColumn([a], "sqrt(a)")
		self.assertEqual(b.name.mem, "sqrt(a)")
		self.assertEqual(b.name.getEqName(), "\\sqrt{a}")
		
		b = calcColumn([a], "a**2")
		self.assertEqual(b.name.mem, "a**2")
		self.assertEqual(b.name.getEqName(), "a^{2}")

	#Special 7: If calcColumn does not assume that certain symbols are real, generating the propagation of uncertainty might fail
	def testSpecial7(self):
		calcColumn([Column(Name("a"),1,[ 17], [1])],"Abs(a)")
		calcColumn([Column(Name("a"),1,[ 17], [1])],"10**a")

	#Special 8: calcColumn needs to do the correct subsitutions for the example Equation, even if we use the Equation.nameDict feature
	def testSpecial8(self):
		a = Column(Name("a"), 1, [17])
		b = Column(Name("b"), 1, [5])
		eq = Equation.fromSympy("x=a*b", nameDict={"k": "a"})
		ret = calcColumn([a, b], eq)
		#exampleEquation should not be x = b k\\= \left(\ensuremath{5}\ensuremath{{.}00}\ensuremath{}\right) k\\=\ensuremath{85}\ensuremath{{.}0}\ensuremath{}
		self.assertEqual(ret.exampleEquation, r"x = b k\\= \left(\ensuremath{5}\ensuremath{{.}00}\ensuremath{}\right) \left(\ensuremath{17}\ensuremath{{.}0}\ensuremath{}\right)\\=\ensuremath{85}\ensuremath{{.}0}\ensuremath{}")

	#Special 9: latex(symbols("a_b"))=="a_{b}" . This results in currently unsolved problems
	def testSpecial9(self):
		col = Column(Name("c"), 1, [17])
		eq = Equation.fromSympy("x=c", nameDict={"a_b": "c"})
		ret = calcColumn([col], eq)
		self.assertEqual(ret.exampleEquation, r"x = a_b\\= \left(\ensuremath{17}\ensuremath{{.}0}\ensuremath{}\right)\\=\ensuremath{17}\ensuremath{{.}0}\ensuremath{}")
		
	def test_checkGetUnit(self):
		goodPairs = [[7.4*sympy.physics.units.meter, sympy.physics.units.meter],
			     [1.0*sympy.physics.units.meter, sympy.physics.units.meter],
			     [1*sympy.physics.units.meter, sympy.physics.units.meter],
			     [5*sympy.physics.units.meter, sympy.physics.units.meter],
			     [sympy.physics.units.meter, sympy.physics.units.meter],
			     [3*sympy.physics.units.centimeter + sympy.physics.units.meter, sympy.physics.units.meter, sympy.physics.units.centimeter],
			     [sympy.sin(volt*coulomb/joule),1]]

		for pair in goodPairs:
			flag = False
			u = checkGetUnit(pair[0])
			for i in range(1, len(pair)):
				if u == pair[i]:
					flag = True
			assert flag,str(u)+" is not the correct unit of:\n"+str(pair[0])
	def test_Column_val(self):
		col = Column(Name("x"), 1, [123], rawDelta=[2])
		self.assertEqual(col.val(numDigits=0, showDelta=True), r"\ensuremath{123}\ensuremath{(2)}\ensuremath{}")
		self.assertEqual(col.val(numDigits=1, showDelta=True), r"\ensuremath{1}\ensuremath{(1)}\ensuremath{\cdot 10^{2}}")
		self.assertEqual(col.val(numDigits=2, showDelta=True), r"\ensuremath{1}\ensuremath{{.}2(1)}\ensuremath{\cdot 10^{2}}")
		self.assertEqual(col.val(numDigits=3, showDelta=True), r"\ensuremath{123}\ensuremath{(2)}\ensuremath{}")
		self.assertEqual(col.val(numDigits=4, showDelta=True), r"\ensuremath{123}\ensuremath{{.}0(20)}\ensuremath{}")
	def test_PlotLine_fitSym(self):
		pl = PlotLine(None, Column(Name("t"), second, [1,2,3,4,5]), Column(Name("s"), meter,[17+11,17+19,17+33,17+40.7,17+48]), False,False)
		pl.fitSym("s=a*t+b",{"a":meter/second,"b":meter})
		self.assertFloatEqual(9.57000001619353, pl.poptcolStr["a"].rawData[0])
		self.assertFloatEqual(18.6299999514194, pl.poptcolStr["b"].rawData[0])
		self.assertFloatEqual(0.6598737167481443, pl.poptcolStr["a"].rawDelta[0])
		self.assertFloatEqual(2.188553539405487, pl.poptcolStr["b"].rawDelta[0])
	def test_propagation_of_uncertainty(self):
		colx = Column(Name("x"), 1, [4], rawDelta=[0.2])
		coly = Column(Name("y"), 1, [7], rawDelta=[0.3])
		ret = calcColumn([colx, coly], "-x**3*y**2", useBetterProp=False)
		# |d/dx| = 3 x^2 y^2 = 2352
		# |d/dy| = 2 x^3 y = 896
		# delta = 0.2 * 2352 + 0.3 * 896 = 739.2
		self.assertFloatEqual(ret.rawDelta[0], 739.2)
			
	from src.utils import test_replaceTexSymbol



#TODO: sympy to beautiful latex converter :
#X_C = 1000*ohm * U_C / U_R

print("TODO: unitTest_withoutUnit()")
#unitTest_withoutUnit()

if __name__ == '__main__':
	unittest.main()

#python -m unittest unitTests.MyTests.testSpecial8

#python -m coverage run --source src --omit "src/prettylatex.py,src/inactive.py" -m unittest unitTests.py
#python -m coverage report
#python -m coverage html
#firefox htmlcov/index.html