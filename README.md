QASDAD, the quick and simple data analysis and documentation program.

QASDAD is intended to speed up and simplify the process of analysing the data of
physical experiments and documenting it and make that process less error prone.
You can find examples that are intended to show the capabilities of QASDAD in 
the "examples" directory, but most of them are broken.
You can find a german tutorial in the "german tutorial" directory, but it is
currently broken.
The documentation can be generated, by running "doxygen" in the "docu" directory
and "make" in the "docu/latex" directory.
"docu/latex/refman.pdf" is the documentation in pdf-format.
You should note that QASDAD is still under development, super buggy and not 
feature complete, but feel free to contact me for bug reports and feature 
requests at volker.weissmann@gmx.de .
The public API is not stable yet, so feel free to propose API changes.


QASDAD is written by Volker Weißmann, a German physics student at the University 
of Stuttgart.
For licensing issues, bug reports, feature requests etc. 
contact me at volker.weissmann@gmx.de . 
This program is free software (free as in freedom) 
and you can find the full license of this program in LICENSE.TXT
The direct dependencies of QASDAD are
* Python 3 (https://www.python.org , not included, needs to be installed separately)
* Scipy (https://www.scipy.org , not included, needs to be installed separately)
* Numpy (http://www.numpy.org , not included, needs to be installed separately)
* Matplotlib (https://matplotlib.org/ , not included, needs to be installed separately)
* Sympy (https://www.sympy.org , not included, needs to be installed separately)
* latex2sympy (https://gitlab.com/volkerweissmann/latex2sympy , not included, 
needs to be in the same folder as qasdad)


Good Luck.

QASDAD, the quick and simple data analysis and documentation program
Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
