#!/usr/bin/env python3
import os
import sys
import shutil

def comparePdfs(fileA, fileB):
	fa = open(fileA, "rb")
	ca = fa.read()
	fb = open(fileB, "rb")
	cb = fb.read()
	ignore = False
	for i in range(0, len(ca)):
		if ca[i:i+len("/CreationDate")] == b"/CreationDate":
			ignore = True
		elif ca[i] == b")":
			ignore = False
		if not ignore and ca[i] != cb[i]:
			# breaker = False
			# for a in range(10, -1, -1):
			# 	#print(ca[i-0:i+1].decode("utf-8"))
			# 	for b in range(10, -1, -1):
			# 		try:
			# 			print(ca[i-a:i+b].decode("utf-8"))
			# 		except:
			# 			pass
			# 		else:
			# 			print(a,b)
			# 			breaker = True
			# 		if breaker:
			# 			break
			# 	if breaker:
			# 		break
			# breaker = False
			# for a in range(10, -1, -1):
			# 	#print(ca[i-0:i+1].decode("utf-8"))
			# 	for b in range(10, -1, -1):
			# 		try:
			# 			print(cb[i-a:i+b].decode("utf-8"))
			# 		except:
			# 			pass
			# 		else:
			# 			print(a,b)
			# 			breaker = True
			# 		if breaker:
			# 			break
			# 	if breaker:
			# 		break
			return False
			#exit(1)
	return True
def printUnEqualTree(dirA, dirB):
	#os.system("diff -rqN " + str(dirA) + " " + str(dirB))
	#return
	print("comparing: ", dirA, " and ", dirB)
	for subdir, dirs, files in os.walk(dirA):
		for file in files:
			apath = os.path.join(subdir, file)
			rel = os.path.relpath(apath, start=dirA)
			bpath = os.path.join(dirB, rel)
			if file.endswith(".pdf"):
				if not comparePdfs(apath, bpath):
					print("pdfs are different: ", os.path.relpath(apath, start=dirA))
			else:
				fa = open(apath, "rb")
				ca = fa.read()
				fa.close()
				fb = open(bpath, "rb")
				cb = fb.read()
				fb.close()
				if ca != cb:
					print("files are different: ", os.path.relpath(apath, start=dirA))
	for subdir, dirs, files in os.walk(dirB):
		for file in files:
			bpath = os.path.join(subdir, file)
			#f = open(bpath)
			#f.close()
			rel = os.path.relpath(bpath, start=dirB)
			apath = os.path.join(dirA, rel)
			try:
				f = open(apath)
				f.close()
			except:
				print("new file: ", os.path.relpath(apath, start=dirA))

#for subdir, dirs, files in os.walk(SOURCE_PATH):
#       for file in files:
#               sourcepath = os.path.join(subdir, file)

checkpath = sys.path[0]

for name in os.listdir(checkpath):
	if os.path.isdir(os.path.join(checkpath, name)):
		if len(sys.argv) == 1 or sys.argv[1] == name:
			try:
				shutil.rmtree(os.path.join(checkpath, name, "prot_qasdad_output"))
			except:
				pass
			#if os.system(str(os.path.join(os.path.dirname(sys.argv[0]), "../../qasdadp.py")) +  " " + str(os.path.join(checkpath, name, "prot")) ) != 0: #+ " > /dev/null"
			if os.system("sqasdad" +  " " + str(os.path.join(checkpath, name, "prot")) ) != 0: #+ " > /dev/null"
				print("QASDAD RETURNED WITH A NON ZERO EXIT STATE")
				exit(1)
			os.system("rm -r " + os.path.join(checkpath, name, "prot_qasdad_output", "qasdad_out", "qasdad"))
			#shutil.rmtree(os.path.join(checkpath, name, "prot_qasdad_output", "qasdad_out", "qasdad"))
			printUnEqualTree(os.path.join(checkpath, name, "orig_output"), os.path.join(checkpath, name, "prot_qasdad_output"))
#if os.path.isdir(os.path.join(a_dir, name))]
