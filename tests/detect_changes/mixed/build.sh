#!/bin/bash

cd ${0%/*} || exit 1
sqasdad prot || exit 1
cd prot_qasdad_output
texfot pdflatex -interaction=nonstopmode -file-line-error -halt-on-error main.tex
evince main.pdf &