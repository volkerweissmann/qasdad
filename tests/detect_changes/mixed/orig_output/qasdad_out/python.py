from qasdad import *
setDataPath("qasdad_out")
setCachePath("../prot_qasdad_cache")
setLatexPath(".")
setNoTexPlot(False)
setNoSaveFig(False)

setTexFile(open("qasdad_out/2.0.tex",'w'))
vals = [0.0000123456789, 0.000123456789, 0.00123456789, 0.0123456789, 0.0123456789, 0.123456789, 0, 1.23456789, 12.3456789, 123.456789, 1234.56789, 12345.6789]
length = len(vals)
for i in range(0, length):
	vals.append(-vals[i])
deltas = [2*0.0000123456789, 1/2*0.000123456789, 2*0.00123456789, 1/2*0.0123456789, 2*0.0123456789, 1/2*0.123456789, 2*1.23456789, 1/2*12.3456789, 2*123.456789, 1/2*1234.56789, 2*12345.6789]

for digits in range(0,6):
	tex("\\begin{tabular}{", "l"+"|l"*len(deltas), "}")
	tex("digits=",digits)
	for delt in deltas:
		tex("&")
		tex("%.4f" % delt)
	tex("\\\\\\hline\n")
	for val in vals:
		tex(val)
		for delt in deltas:
			tex("&")
			try:
				tex(ValueFormat(digits, True).toNiceTexString(val, delt))
			except Exception as ex:
				print(ex)
				tex("raised")
		tex("\\\\\n")
	tex("\\end{tabular}\\\\\\\\")

