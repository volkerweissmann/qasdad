#while(1) {
#plot 	"1to2" using 1:2 with lines,\
#	"2to4" using 1:2 with lines,\
#	"4to8" using 1:2 with lines,\
#	"8toMono" using 1:2 with lines,\
#	"monoToDouble" using 1:2 with lines,\
#	"doubleToOrbit" using 1:2 with lines
#pause 1
#}

while(1) {
plot 	"1to2" using 1:2,\
	"2to4" using 1:2,\
	"4to8" using 1:2,\
	"8toMono" using 1:2,\
	"monoToDouble" using 1:2,\
	"doubleToOrbit" using 1:2
pause 1
}
