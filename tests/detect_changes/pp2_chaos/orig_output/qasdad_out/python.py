from qasdad import *
setDataPath("qasdad_out")
setLatexPath(".")
setNoTexPlot(False)
setNoSaveFig(False)
setTexFile(open("qasdad_out/1.tex",'w'))

table = readFile(os.path.join("data", "Pendel 0,9Hz 1erPer.txt"))
for i in range(0, len(table["x"].rawData)):
    table["x"].rawData[i] /= 10
for i in range(0, len(table["v"].rawData)):
    table["v"].rawData[i] *= -30
p = Plot()
p.add(table["t"], table["x"], xerr=False, yerr=False)
p.add(table["t"], table["v"], xerr=False, yerr=False)
p.ax.set_ylabel("Not calibrated")
p.ax.yaxis.set_major_locator(matplotlib.ticker.NullLocator())
p.showAsFigure("fig:pend1","Measured position $x$ and velocity $v$ of the mechanical pendulum. You can see that there is one maximum in every period.")


table = readFile(os.path.join("data","Pendel 0,2Hz 2erPer.txt"))
for i in range(0, len(table["x"].rawData)):
    table["x"].rawData[i] /= 1
p = Plot()
p.add(table["t"], table["x"], xerr=False, yerr=False)
#p.add("", table["t"], table["v"], xerr=False, yerr=False)
p.ax.set_ylabel("Not calibrated")
p.ax.yaxis.set_major_locator(matplotlib.ticker.NullLocator())
p.showAsFigure("fig:pend2","Measured position $x$ of the mechanical pendulum. You can see that there are two maxima in every period.")

tex("\\subsection{States of the shinriki oscillator}")
tex("We measured the phase space diagram of the shinriki oscillator for many different  values of $R_1$ and $R_2$ but decided to show these only for a small number of selected values of $R_1$ and $R_2$ to prevent this paper from having to many pages. We plotted $U_1(t)$, $U_2(t)$, the phase space diagram, the discrete Fourier transform and the autocorrelation in the figures \\ref{fig:shinriki0}, \\ref{fig:shinriki1}, \\ref{fig:shinriki2}, \\ref{fig:shinriki3} and \\ref{fig:shinriki4}.")
labelIndex = 0
for fpath in [os.path.join("data","feigenbaum", "40 42.txt"),
              os.path.join("data","feigenbaum", "55.2 42.txt"),
              os.path.join("data","feigenbaum", "55.4 42.txt"),
              os.path.join("data","feigenbaum", "64 42.txt"),
              os.path.join("data","feigenbaum", "73 42.txt")]:
    tex("\\begin{figure}[H]")
    table = readFile(fpath)
    table["U_1"].rawData = np.array(table["U_1"].rawData)/max(table["U_1"].rawData)*10
    table["U_2"].rawData = np.array(table["U_2"].rawData)/max(table["U_2"].rawData)*10

    p = Plot(size=(4,3))
    p.add(table["t"], table["U_1"])
    p.add(table["t"], table["U_2"])
    #p.ax.set_xlim(0,10)
    p.ax.set_ylabel("$U$ [V]")
    p.ax.set_xlabel("$t$ [ms]", usetex=True)
    tex("\\subfloat[$U_1(t)$ and $U_2(t)$]{{")
    p.showHere(0.5, loc="upper right")
    tex("}}")

    p = Plot(size=(4,3))
    p.add(table["U_1"], table["U_2"], label="")
    #p.ax.set_xlabel("$U$")
    #p.ax.set_ylabel("$U$")
    tex("\\subfloat[phase space diagram]{{")
    p.showHere(0.5)
    tex("}}")

    table.add(calcNfft(table["t"], table["U_2"], "|ft|", "f"))
    p = Plot(size=(4,3))
    index = len(table["f"].rawData)//10
    p.add(table["f"].rawData[:index]*1000, table["|ft|"].rawData[:index], label="|fft|")
    p.ax.set_xlabel("$f$ [Hz]")
    p.ax.set_ylabel("Amplitude [a.u]")
    #p.ax.set_yscale("log") #nonposx='clip'
    tex("\\\\\\subfloat[Absolute value of the Fourier transform of $U_1$]{{")
    p.showHere(0.5)
    tex("}}")

    cor = np.correlate(table["U_2"].rawData, table["U_2"].rawData, mode="full")
    cor = cor[cor.size//2:]
    p = Plot(size=(4,3))
    p.ax.plot(table["t"].rawData, cor/10000)
    p.ax.set_xlabel("t [ms]")
    p.ax.set_ylabel("Autocorrelation($U_2$)", usetex=True)
    #p.ax.yaxis.set_major_locator(matplotlib.ticker.NullLocator())
    tex("\\subfloat[auto correlation of $U_2$]{{")
    p.showHere(0.5)
    tex("}}")
    
    r1 = round(float(os.path.basename(fpath).replace(".txt", "").split(" ")[0])/100,3)
    r2 = round(float(os.path.basename(fpath).replace(".txt", "").split(" ")[1])/100,3)
    tex("\\caption{Diagrams for the shinriki oscillator with $R_1=",r1,"$ and $R_2=", r2, "$.}")
    tex("\\label{fig:shinriki" + str(labelIndex) + "}")
    labelIndex += 1
    tex("\\end{figure}")

tex("\\subsection{Transition points}We also measured the transition points between the different states of the shinriki oscillator. These are shown in the tables \\ref{tab:1to2}, \\ref{tab:2to4}, \\ref{tab:4to8}, \\ref{tab:8toMono}, \\ref{tab:monoToDouble} and \\ref{tab:doubleToOrbit}. We also plotted the transition-curves in figure \\ref{fig:trans}.")
trans1to2 = readFile(os.path.join("data","übergänge","1to2"))
trans2to4 = readFile(os.path.join("data","übergänge","2to4"))
trans4to8 = readFile(os.path.join("data","übergänge","4to8"))
trans8toMono = readFile(os.path.join("data","übergänge","8toMono"))
transMonoToDouble = readFile(os.path.join("data","übergänge","monoToDouble"))
transDoubleToOrbit = readFile(os.path.join("data","übergänge","doubleToOrbit"))
for table in [trans1to2, trans2to4, trans4to8, trans8toMono, transMonoToDouble, transDoubleToOrbit]:
    table.add(calcColumn(table, "R_1*100*ohm", "R_1"))
    table.add(calcColumn(table, "R_2*20*ohm", "R_2"))
showAsTable([trans1to2["R_1"],trans1to2["R_2"]], "tab:1to2", "Measured transition points from one-periodic movement to two-periodic movement.", format={"R_1": ValueFormat(2,0),"R_2": ValueFormat(2,0)})
showAsTable([trans2to4["R_1"],trans2to4["R_2"]], "tab:2to4", "Measured transition points from two-periodic movement to four-periodic movement.", format={"R_1": ValueFormat(2,0),"R_2": ValueFormat(2,0)})
showAsTable([trans4to8["R_1"],trans4to8["R_2"]], "tab:4to8", "Measured transition points from four-periodic movement to eight-periodic movement.", format={"R_1": ValueFormat(2,0),"R_2": ValueFormat(2,0)})
showAsTable([trans8toMono["R_1"],trans8toMono["R_2"]], "tab:8toMono", "Measured transition points from eight-periodic movement to monoscroll chaos.", format={"R_1": ValueFormat(2,0),"R_2": ValueFormat(2,0)})
showAsTable([transMonoToDouble["R_1"],transMonoToDouble["R_2"]], "tab:monoToDouble", "Measured transition points from monoscroll chaos to doublescroll chaos.", format={"R_1": ValueFormat(2,0),"R_2": ValueFormat(2,0)})
showAsTable([transDoubleToOrbit["R_1"],transDoubleToOrbit["R_2"]], "tab:doubleToOrbit", "Measured transition points from doublescroll chaos to the great orbit.", format={"R_1": ValueFormat(2,0),"R_2": ValueFormat(2,0)})
p = Plot()
p.add(trans1to2["R_1"], trans1to2["R_2"], xerr=False, yerr=False, label="1$\\rightarrow$2")
p.add(trans2to4["R_1"], trans2to4["R_2"], xerr=False, yerr=False, label="2$\\rightarrow$4")
p.add(trans4to8["R_1"], trans4to8["R_2"], xerr=False, yerr=False, label="4$\\rightarrow$8")
p.add(trans8toMono["R_1"], trans8toMono["R_2"], xerr=False, yerr=False, label="8$\\rightarrow$Mono")
p.add(transMonoToDouble["R_1"], transMonoToDouble["R_2"], xerr=False, yerr=False, label="Mono$\\rightarrow$Double")
p.add(transDoubleToOrbit["R_1"], transDoubleToOrbit["R_2"], xerr=False, yerr=False, label="Double$\\rightarrow$Orbit")
p.ax.set_xlabel("$R_1$ [k$\\Omega$]")
p.ax.set_ylabel("$R_2$ [k$\\Omega$]")
p.showAsFigure("fig:trans", "This diagram shows the measured state Transistions from 1-periodic to 2-periodic to 4-periodic to 8-periodic to monoscroll-chaos to doublscroll-chaos to the great orbit.") #The x- and y-axis display the percentage of the resistors.


tex("\\subsection{Feigenbaum diagram}")
maximaX = []
maximaY = []
r1s = []
for subdir, dirs, files in os.walk(os.path.join("data","feigenbaum")):
    for file in files:
        #testGetMaxima(os.path.join(subdir, file), "t", "U_1")
        table = readFile(os.path.join(subdir, file))
        r1 = float(file[:-6])
        if r1 < 42:
            for i in range(0,len(table["U_2"].rawData)):
                table["U_2"].rawData[i] /= 1000
        if r1 < 55.6:
            for i in range(0,len(table["U_1"].rawData)):
                table["U_1"].rawData[i] /= 1000
        
        r1 *= 100/100
        r1s.append(r1)
        x,y = getMaxima(table["U_1"])
        for m in y:
            maximaX.append(r1)
            maximaY.append(m)
r1s.sort()
tex("To construct the feigenbaum diagram, the maximas were measured for $R_2=0.42$ and the following $R_1$: ")
begin = False
for r1 in r1s:
    if begin:            
        tex(round(r1/100,3))
        begin = False
    else:
        tex("\\item ", round(r1,3))
        tex(" k$\\Upomega$\n")
    
p = Plot(display=False)
p.ax.plot(maximaX, maximaY, linestyle="", marker="+")

print(len(maximaX))

tout = Table()
tout.add(Column("R_1", "eq", 1, maximaX, rawDelta=np.ones(len(maximaY))))
tout.add(Column("U_1", "eq", 1, maximaY, rawDelta=np.ones(len(maximaX))))
tex("================================\n")
showAsTabular([tout["R_1"], tout["U_1"]], format={"R_1": ValueFormat(3, False), "U_1": ValueFormat(3, False)})
tex("================================\n")

print(maximaX)
print(maximaY)

p.ax.set_xlabel("$R_1$ [k$\\Omega$]")
p.ax.set_ylabel("maxima($U_2$) [V]", usetex=True)
tex(". If we plot the measured maximas against $R_2$ we get the feigenbaum diagram as shown in figure \\ref{fig:feigenbaum}.")
p.showAsFigure("fig:feigenbaum", "Feigenbaum diagram for the shinriki oscillator.")
tex("\\subsection{Feigenbaum constant}\\label{sec:messConst}Because the measured values of $R_1$ in the Feigenbaum diagram are not that exact, we launched a second attempt to measure the first three bifurcation points: We measured the first bifurkation point at $R_1=84.8\\text{ [k}\\Upomega\\text{]}, R_2=38.6\\text{ [k}\\Upomega\\text{]}$, the second bifurkation point at $R_1=95.8\\text{ [k}\\Upomega\\text{]}, R_2=38.6\\text{ [k}\\Upomega\\text{]}$ and the third bifurkation point at $R_1=98.4\\text{ [k}\\Upomega\\text{]},R_2=38.6\\text{ [k}\\Upomega\\text{]}$.")

for fpath in [os.path.join("data", "Pendel 0,9Hz 1erPer.txt"),
              os.path.join("data","Pendel 0,2Hz 2erPer.txt")]:
    table = readFile(fpath)
    if os.path.basename(fpath) == "Pendel 0,9Hz 1erPer.txt":
        for i in range(0, len(table["x"].rawData)):
            table["x"].rawData[i] /= 100*4
        for i in range(0, len(table["v"].rawData)):
            table["v"].rawData[i] *= -3/4
    else:
        for i in range(0, len(table["x"].rawData)):
            table["x"].rawData[i] /= 100
        for i in range(0, len(table["v"].rawData)):
            table["v"].rawData[i] /= 200
    tex("\\begin{figure}[H]")
    #table["U_1"].rawData = np.array(table["U_1"].rawData)/max(table["U_1"].rawData)*10
    #table["U_2"].rawData = np.array(table["U_2"].rawData)/max(table["U_2"].rawData)*10

    p = Plot(size=(4,3))
    p.add(table["t"], table["x"])
    p.add(table["t"], table["v"])
    #p.ax.set_xlim(0,10)
    p.ax.set_ylabel("[a.u]")
    tex("\\subfloat[$U_1(t)$ and $U_2(t)$]{{")
    p.showHere(0.5, loc="upper right")
    tex("}}")
    print("==================== phase space===========")
    p = Plot(size=(4,3))
    p.add(table["x"], table["v"], label="")
    #p.ax.set_xlabel("$U$")
    #p.ax.set_ylabel("$U$")
    p.ax.set_xlabel("[a.u]")
    p.ax.set_ylabel("[a.u]")
    tex("\\subfloat[phase space diagram]{{")
    p.showHere(0.5)
    tex("}}")

    table.add(calcNfft(table["t"], table["x"], "|ft|", "f"))
    p = Plot(size=(4,3))
    index = len(table["f"].rawData)//20
    if os.path.basename(fpath) == "Pendel 0,9Hz 1erPer.txt":
        index //= 5
    p.add(table["f"].rawData[:index], table["|ft|"].rawData[:index], label="|fft|")
    p.ax.set_xlabel("$f$ [Hz]", usetex=True)
    p.ax.set_ylabel("Amplitude [a.u]")
    #p.ax.set_yscale("log") #nonposx='clip'
    tex("\\\\\\subfloat[Absolute value of the Fourier transform of $U_1$]{{")
    p.showHere(0.5)
    tex("}}")
    print("==================== abc===========")
    cor = np.correlate(table["x"].rawData, table["x"].rawData, mode="full")
    cor = cor[cor.size//2:]
    p = Plot(size=(4,3))
    p.ax.plot(table["t"].rawData, cor/10000)
    p.ax.set_xlabel("$t$ [s]", usetex=True)
    p.ax.set_ylabel("Autocorrelation($x$)", usetex=True)
    #p.ax.yaxis.set_major_locator(matplotlib.ticker.NullLocator())
    tex("\\subfloat[auto correlation of $v$]{{")
    p.showHere(0.5)
    tex("}}")
    
    r1 = 0#round(float(os.path.basename(fpath).replace(".txt", "").split(" ")[0])/100,3)
    r2 = 0#round(float(os.path.basename(fpath).replace(".txt", "").split(" ")[1])/100,3)
    tex("\\caption{Diagrams for the shinriki oscillator with $R_1=",r1,"$ and $R_2=", r2, "$.}")
    tex("\\end{figure}")


