#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

##Column is intended to hold a physical property that has different values in different repititions of the same experiment\n
#self.name is the name of this Column. Instance of the Name class.\n
#self.basicSIUnit is the unit of the physical property. Has to be 1 in SI units (e.g. meter*kilogram is allowed, but inch is not)\n
#self.rawData is a numpy array hat holds the value of the physical property (without a unit) rawData*basicSIUnit is the value of the physical property\n
#self.rawDelta is  a numpy array that holds the measuring inaccuracy (without a unit) rawDelta*basicSIUnit is the measuring inaccuracy\n
#self.rawDelta may be None\n
#If self.rawDelta is not None the lengths of self.rawData and self.rawDelta have to be the same\n
class Column:
	#! [Column data]
	##\snippet this Column data
	##\return Value of the physical property including its unit
	def data(self, index):
		return self.rawData[index]*self.basicSIUnit
	#! [Column data]
	#! [Column delta]
	##\snippet this Column delta
	##\return Uncertainty of the physical property including its unit
	def delta(self, index):
		return self.rawDelta[index]*self.basicSIUnit
	#! [Column delta]
	#@cond INTERNAL
	##\return Returns True if the physical property is exact
	def isDeltaZero(self):
		if self.rawDelta is None:
			return True
		for i in self.rawDelta:
			if i != 0:
				return False
		raise ValueError("rawDelta is 0, but should be None")
	def deepcopy(self):
		return Column(self.name, self.basicSIUnit, self.rawData.copy(), self.rawDelta.copy())
	#@endcond INTERNAL
	##Changes self.basicSIUnit to an equivalent one
	def newbsiUnit(self, new):
		assert(subsSIUnitsOne(new)==1)
		if toBasicSI(new) == toBasicSI(self.basicSIUnit):
			self.basicSIUnit = new
			self.prefUnitTex = anythingExceptNumbersToTex(new)
		else:
			raise IncompDimensions(str(self.basicSIUnit) + " != " + str(new))
	##Constructor: Sets member variables according to arguments
	def __init__(self, name, basicSIUnit, rawData, rawDelta=None, prefUnitTex=None, prefUnitFac=1):#TODO Should we check, wether rawData and rawDelta are made up of floats?
		assert(subsSIUnitsOne(basicSIUnit)==1)
		if not isinstance(name, Name):
			raise TypeError("name in Column.__init__ should be an instance of Name")
		if subsSIUnitsOne(basicSIUnit) != 1:
			raise ValueError("subsSIUnitsOne(basicSIUnit) != 1")
		if isinstance(rawData, list):
			rawData = np.array(rawData)
		elif not isinstance(rawData, np.ndarray):
			raise ValueError("rawData should be a numpy ndarray but is of type: ", type(rawData))
		if isinstance(rawDelta, list):
			rawDelta = np.array(rawDelta)
		elif rawDelta is not None and not isinstance(rawDelta, np.ndarray):
			raise ValueError("rawDelta should be None or a numpy ndarray but is of type: ", type(rawDelta))
		# if rawDelta is not None and np.min(rawDelta) < 0:
		# 	raise ValueError("rawDelta has some negative Values", rawDalta)
		#if preferredUnit is None:
		#	preferredUnit = basicSIUnit
		#elif basicSIUnit != checkGetUnit(toBasicSI(preferredUnit)):
		#	raise IncompDimensions("preferedUnit has the wrong dimensions")
		self.name = name
		self.basicSIUnit = basicSIUnit
		#self.preferredUnit = preferredUnit
		#self.prefUnitTex = anythingExceptNumbersToTex(preferredUnit)
		#self.prefUnitFac = float(basicSIUnit/preferredUnit)
		if prefUnitTex is None:
			prefUnitTex = anythingExceptNumbersToTex(basicSIUnit)
		self.prefUnitTex = prefUnitTex
		self.prefUnitFac = prefUnitFac
		self.rawData = rawData
		self.rawDelta = rawDelta
		self.exampleEquation = None
		self.examplePref = None
	#todo: really equivalent?
	##Equivalent to ValueFormat(numDigits,showDelta).toNiceTexString(self.rawData[pos]*self.basicSIUnit, self.rawDelta[pos]*self.basicSIUnit) behaves like pos = 0 if pos is None and len(self.rawData)==1
	def val(self, pos=None, numDigits=0, showDelta=False):
		assert(isinstance(numDigits, int))
		if pos == None:
			if  len(self.rawData) == 1:
				pos = 0
			else:
				raise ValueError("pos cant be None if the column consist of more than one value")
		if numDigits == 0 or showDelta == True:
			numstr = ValueFormat(numDigits=numDigits, showDelta=showDelta).toNiceTexString(self.rawData[pos]/self.prefUnitFac, self.rawDelta[pos]/self.prefUnitFac)
		else:
			numstr = ValueFormat(numDigits=numDigits, showDelta=showDelta).toNiceTexString(self.rawData[pos]/self.prefUnitFac)
		if self.basicSIUnit == 1:
			return numstr
		else:
			return numstr + r"\,\,\ensuremath{" + self.prefUnitTex + "}" #anythingExceptNumbersToTex(self.basicSIUnit)
	##Writes a nice example calculation on how this column was calculated
	#\param label: latex label of the equation in case you want to refer to this example calculation
	def exampleCalc(self, label=None):
		#if label is None:
		#	tex(r"\begin{equation}\begin{aligned}", self.exampleCalc, r"\end{aligned}\end{equation}")
		#else:
		#	tex(r"\begin{equation}\begin{aligned}", self.exampleCalc, "\end{aligned}\\label{", str(label), r"}\end{equation}")
		tex(self.examplePref)
		if label is None:
			tex(r"\begin{dmath}", self.exampleEquation, r"\end{dmath}") #\usepackage{breqn}
		else:
			tex(r"\begin{dmath}[label={", label, "}]", self.exampleEquation, r"\end{dmath}")
	##Writes a nice propagation of uncertainty of how this column was calculated
	#\param label: latex label of the equation in case you want to refer to this calculation
	def propUncertEquation(self, label=None):
		if label is None:
			tex(r"\begin{equation}\begin{aligned}", self.analPropUncert, r"\end{aligned}\end{equation}")
		else:
			tex(r"\begin{equation}\begin{aligned}", self.analPropUncert, "\end{aligned}\\label{", str(label), r"}\end{equation}")
		#if label is None:
		#	tex(r"\begin{dmath}", self.analPropUncert, r"\end{dmath}")
		#else:
		#	tex(r"\begin{dmath}[label={", label, "}]", self.analPropUncert, r"\end{dmath}")
#@cond INTERNAL
	##Only Used for debug purposes, exact format is not guaranteed to be stable
	def __str__(self):
		ret = "<" + self.name.mem
		for i  in self.rawData:
			ret += " " + str(i) #self.format.format(i)
		ret += ">"
		return ret
	##Only Used for debug purposes, exact format is not guaranteed to be stable
	def __repr__(self):
		return "Column(" + repr(self.name) + "," + repr(self.basicSIUnit) + ", np." + repr(self.rawData) + ", np." + repr(self.rawDelta) + ", " + repr(self.prefUnitTex) + ", " + repr(self.prefUnitFac) + ")"
	##Checks whether this Column is badly formatted. If so, raise
	def raiseIfBad(self):
		if not isinstance(self.name, Name):
			raise TypeError("Column.name is of type:", type(self.name))
		self.name.raiseIfBad()
		if self.basicSIUnit is None:
			raise ValueError("Column.basicSIUnit is None")
		if not isinstance(self.rawData, np.ndarray):
			raise TypeError("Column.rawData is of type:", type(self.rawData))
		if self.rawDelta is not None and not isinstance(self.rawDelta, np.ndarray):
			raise TypeError("Column.rawData is of type:", type(self.rawData))
		if self.rawDelta is not None and self.rawData.shape != self.rawDelta.shape:
			raise ValueError("shapes of Column.rawData and Column.rawDelta do not match. Column.name="+repr(self.name)+str(self.rawData)+str(self.rawData))
		if len(self.rawData.shape) != 1:
			raise ValueError("Column.rawData has more than one dimension")
		for i in self.rawData:
			if not isinstance(i, (np.float64, np.int64)): 
				raise TypeError("Bad type in Column.rawData: ", type(i))
		if self.rawDelta is not None:
			for i in self.rawDelta:
				if not isinstance(i, (np.float64, np.int64, np.float32, np.int32)): #sympy.numbers.Float would make problem in calcColumn ret.rawDelta += table[str(s)].rawDelta * diffDictNumpy[str(s)](*raw_datas_for_all_symbols)
					raise TypeError("Bad type in Column.rawDelta: ", type(i))
				if i <= 0:
					raise ValueError("Element of self.rawDelta is smaller or equal to zero. name.mem=", self.name.mem, " rawDelta:", str(i))
#@endcond INTERNAL