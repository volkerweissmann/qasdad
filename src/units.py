#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

##Exception that occurs if qasdad encounters incompatible dimensions, eg. 3*meter+5*second
class IncompDimensions(Exception):
	pass

#@cond INTERNAL
#! [toBasicSI]
##\snippet this toBasicSI
def toBasicSI(expr):
	#if isinstance(expr, list):
	#	return [toBasicSI(e) for e in expr]
	if isinstance(expr, dict):
		return dict([[k,toBasicSI(v)] for k,v in expr.items()])
	return sympy.physics.units.convert_to(expr,[sympy.physics.units.meter,sympy.physics.units.second,sympy.physics.units.kilogram,sympy.physics.units.coulomb,sympy.physics.units.kelvin])
#! [toBasicSI]

#! [subsSIUnitsOne]
##\snippet this subsSIUnitsOne
def subsSIUnitsOne(expr):
	#if isinstance(expr, list):
	#	return [subsSIUnitsOne(e) for e in expr]
	expr = toBasicSI(expr).subs({
		sympy.physics.units.meter:1,
		sympy.physics.units.second:1,
		sympy.physics.units.kilogram:1,
		sympy.physics.units.coulomb:1,
		sympy.physics.units.kelvin:1,
	})
	return expr
#! [subsSIUnitsOne]

def makeRaw(exprList):
	return np.array([np.float(subsSIUnitsOne(expr)) for expr in exprList])

def checkGetUnitBackend(a, unitdict):
	try:
		float(a)
		return 1
	except:
		pass
        #TODO diese funktion dokumentiern und im source code nach sympy nach schauen welche isinstance befehler da noch fehlern
	if isinstance(a,sympy.add.Add):
		u = checkGetUnitBackend(a.args[0], unitdict)
		for i in range(1,len(a.args)):
			#print(a.args[i])
			#print(unit(toBasicSI(u))) meter
			#print(checkGetUnit(toBasicSI(u))) meter
			#print(toBasicSI(checkGetUnitBackend(a.args[i])))
			#if unit(toBasicSI(checkGetUnitBackend(a.args[i]))) != unit(toBasicSI(u)):
			#if checkGetUnitBackend(toBasicSI(a.args[i]), unitdict) != checkGetUnitBackend(toBasicSI(u), unitdict): #man beachte dass es den Fall gibt, dass z.B. die linke Seite dieser Gleichung 1 ist und u gleich second/coulomb*I ist und unitdict I=ampere enthält.
			if checkGetUnitBackend(toBasicSI(checkGetUnitBackend(a.args[i], unitdict)), {}) != checkGetUnitBackend(toBasicSI(checkGetUnitBackend(u, unitdict)), {}):
				print("=============================")
				for arg in a.args:
					print(arg)
				print("=============================")
				print(checkGetUnitBackend(toBasicSI(checkGetUnitBackend(a.args[i], unitdict)), {}))
				print(checkGetUnitBackend(toBasicSI(checkGetUnitBackend(        u, unitdict)), {}))
				raise IncompDimensions("incompatible dimensionen")
		return u
	elif isinstance(a,sympy.mul.Mul):
		u = 1
		for arg in a.args:
			u *= checkGetUnitBackend(arg, unitdict)
		return u
	elif isinstance(a, sympy.power.Pow):
		if checkGetUnitBackend(toBasicSI(a.args[1]), toBasicSI(unitdict)) != 1:
			raise IncompDimensions("Incompatible dimensionen")
		return checkGetUnitBackend(a.args[0], unitdict)**a.args[1]
	elif (isinstance (a, (sympy.sin, sympy.asin, sympy.cos, sympy.acos, sympy.tan, sympy.atan, sympy.exp))):
		if checkGetUnitBackend(toBasicSI(a.args[0]), toBasicSI(unitdict)) != 1:
			raise IncompDimensions("Incompatible dimension: The following has the unit:\n"+str(checkGetUnitBackend(a.args[0], unitdict))+"\nbut should be dimensionless:\n"+str(a.args[0]))
		return 1
	elif isinstance(a, sympy.log):
		for arg in a.args:
			checkGetUnitBackend(arg, unitdict)
		return 1
	elif isinstance(a, sympy.atan2):
		if checkGetUnitBackend(toBasicSI(a.args[0]), toBasicSI(unitdict)) != checkGetUnitBackend(toBasicSI(a.args[1]), toBasicSI(unitdict)):
			raise IncompDimension("Incompatible dimension in atan2")
		else:
			return 1
	elif isinstance(a, sympy.physics.units.quantities.Quantity):
		return a
	elif isinstance(a, sympy.symbol.Symbol): #PERFORMANCE move this case upwoards
		if a in unitdict:
			return unitdict[a]
		else:
			raise FreeSymbolException(str(a))
	elif isinstance(a, sympy.Abs):
		return checkGetUnitBackend(a.args[0], unitdict)
	else:
		raise NotImplementedError("unsupported operation for checkGetUnitBackend:", type(a))
#@endcond INTERNAL

##Returns the unit of expr. Raises IncompDimensions if it encounters incompatible dimensions, eg. 3*meter+5*second
def checkGetUnit(expr, unitdict={}):
	#print("============================")
	#if DEBUG:
	#	print("\tcheckGetUnit:")
	#	print("\t\t", expr)
	try:
		u = checkGetUnitBackend(expr, unitdict)
	except FreeSymbolException as ex:
		raise FreeSymbolException("Unable to check and get unit of:", expr, "Because of the free symbol:", ex)
	#if DEBUG:
	#	print("\t\treturns:", u)
	return u