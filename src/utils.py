#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

TEX_FILE = None

#! [texfile]
##Sets the file that the tex(...) command writes to. Gets called by qasdadp.py
#\snippet this texfile
def setTexFile(texfile):
	global TEX_FILE
	if TEX_FILE is not None:
		TEX_FILE.close()
	TEX_FILE = texfile
#! [texfile]

#@cond INTERNAL
def closetexfile():
	global TEX_FILE
	if TEX_FILE is not None:
		TEX_FILE.close()
atexit.register(closetexfile)
#@endcond INTERNAL

#! [tex]
##\snippet this tex
def tex(*args):
	for a in args:
		TEX_FILE.write(str(a)) 
#! [tex]

##Exception that occurs if there is a free symbol in an expression that should not be there
class FreeSymbolException(Exception):
	pass

#@cond INTERNAL
##Used in roundSympy and debugSympyExpr
typeFunctions = [
	sympy.Eq,
	sympy.Add,
	sympy.Mul,
	sympy.Pow,
	sympy.exp,
	sympy.sin,
	sympy.asin,
	sympy.cos,
	sympy.acos,
	sympy.tan,
	sympy.atan,
	sympy.log,
	sympy.Abs,
	sympy.sign,
	]

##Additional number types are: sympy.numbers.Float , sympy.numbers.Rational, sympy.numbers.Integer, sympy.numbers.NaN
specialSympyNumberTypes = (sympy.numbers.Zero,
			   sympy.numbers.One,
			   sympy.numbers.NegativeOne,
			   sympy.numbers.Half,
			   sympy.numbers.Infinity,
			   sympy.numbers.NegativeInfinity,
			   sympy.numbers.ComplexInfinity,
			   sympy.numbers.Exp1,
			   sympy.numbers.ImaginaryUnit,
			   sympy.numbers.Pi,
			   sympy.numbers.EulerGamma,
			   sympy.numbers.Catalan,
			   sympy.numbers.GoldenRatio,
			   sympy.numbers.TribonacciConstant)
#@endcond INTERNAL

##Prints the sympy expr given as an argument and prints all types
def debugSympyExpr(expr, depth=0):
	print("\t"*depth, type(expr), expr)
	if isinstance(expr, tuple(typeFunctions)):
		for a in expr.args:
			debugSympyExpr(a, depth+1)

##Returns true if expr is nan, returns false otherwise
def isnan(expr):
	if expr == sympy.nan:
		return True
	try:
		if math.isnan(expr):
			return True
	except: pass
	#if expr == math.nan: reicht nicht wegen irgend so einem nan case in numpy
	#	return True
	return False

#@cond INTERNAL

def pyescape(str):
	return str.replace("\\","\\\\").replace("\n","\\n").replace("\t","\\t").replace("\"","\\\"").replace("\'","\\\'")

#! [symspace]
##\snippet this symspace
def symspace(str): #Needed because of special 2
	return symbols(str.replace(" ", "{}"))
#! [symspace]
	#return symbols(str.replace(" ", "~\\!\\!"))

def isLeftTexSplit(tex, pos,indices):
	if pos == 0:
		return True
	if tex[pos-1] in [" ", "(", ")", "=", "}", "/"]:
		return True
	#U_{\mathrm{dif}}=\frac{c}{T - T_{c}}
	if tex[pos-1] == "{" and tex[pos-2] != "_":
		return True
	if indices and tex[pos-1] == "_":
		return True
	return False

def isRightTexSplit(tex, pos,indices):
	if pos == len(tex):
		return True
	if tex[pos] in [" ", "(", ")", "=", "{", "}", "/"]:
		return True
	if indices and tex[pos] == "_":
		return True
	return False

##Similar to tex.replace(sym,new) but e.g replaceTexSymbol("exp(x)", "x", "a") returns "exp(a)" and not "eap(a)"
#\param indices If this is set to True, replaceTexSymbol("a_b", "a", "c") will return "c_b" and replaceTexSymbol("a_b", "b", "c") will return "a_c". If this is set to False, it will return "a_b" in both cases
def replaceTexSymbol(tex, sym, new, indices=False):
	#TODO replaceTexSymbol(r"\alpha\beta", r"\beta", "x") does not work
	i = 0
	while i < len(tex):
		if isLeftTexSplit(tex,i,indices) and i+len(sym) <= len(tex) and tex[i:i+len(sym)] == sym and isRightTexSplit(tex,i+len(sym),indices):
			tex = tex[:i] + new + tex[i+len(sym):]
		i += 1
	return tex

def test_replaceTexSymbol(self):
	self.assertEqual(replaceTexSymbol("a e c","e","b"), "a b c")
	self.assertEqual(replaceTexSymbol("e b c","e","a"), "a b c")
	self.assertEqual(replaceTexSymbol("a b e","e","c"), "a b c")
	self.assertEqual(replaceTexSymbol(" exp x ","x","a"), " exp a ")
	self.assertEqual(replaceTexSymbol("a abc c", "abc", "defgh"), "a defgh c")
	self.assertEqual(replaceTexSymbol("a b a cd a b","a","xyz"), "xyz b xyz cd xyz b")
	self.assertEqual(replaceTexSymbol("a_{b}","b","e"), "a_{b}")
	self.assertEqual(replaceTexSymbol("\\frac{a}{b }","b","e"), "\\frac{a}{e }")
	self.assertEqual(replaceTexSymbol("a_b", "a", "c"), "a_b")
	self.assertEqual(replaceTexSymbol("a_b", "b", "c"), "a_b")
	self.assertEqual(replaceTexSymbol("a_b", "a", "c", True), "c_b")
	self.assertEqual(replaceTexSymbol("a_b", "b", "c", True), "a_c")
#@endcond INTERNAL
