#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

##If DEBUG_CHECK_COLS is set to True, some functions that take Columns as arguments  will check if these Columns are well-formatted.
DEBUG_CHECK_COLS=True

import time
#@cond INTERNAL
startTimeMillis = int(round(time.time() * 1000))
#@endcond INTERNAL
##Returns milliseconds since program startup
def millis():
	return int(round(time.time() * 1000))-startTimeMillis

import sympy
from sympy import symbols
import sympy.physics
from sympy.physics.units import meter, second, hertz, kilogram, coulomb, volt, ampere, ohm, henry, kelvin, newton, joule, siemens, farad, watt, tesla
#from sympy.core import AtomicExpr
import numpy as np
import scipy.optimize
import math
import io #used in readFile
import os #used in Plot.showHere
import threading
#from enum import Enum
import matplotlib 
import matplotlib.pyplot #as plt
import hashlib
import atexit
import pickle
import copy
#import nfft
import latex2sympy
import re

from .prettylatex import *
from .utils import *
from .name import *
from .units import *
from .constants import *
from .advutils import *
from .tolatex import *
from .column import *
from .table import *
from .main import *
from .equation import *
from .plotline import *
from .plot import *
from .calcColumn import *
from .aliases import *