#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

#@cond INTERNAL
def non_commutative_sympify(expr_string):
    parsed_expr = sympy.parsing.sympy_parser.parse_expr(
        expr_string, 
        evaluate=False
    )

    new_locals = {sym.name:sympy.Symbol(sym.name, commutative=False)
                  for sym in parsed_expr.atoms(sympy.Symbol)}

    return sympy.sympify(expr_string, locals=new_locals)

##Convert a string to a sympy expression. Supports equal signs every unit from good_known_units_long
def strToSympy(string): #TODO: move this to main.py
	if not isinstance(string, str):
		return string
	ar = string.split("=")
	#expr = [sympy.sympify(ex, evaluate=False).subs(sympy.sqrt(-1), sympy.symbols("I")) for ex in expr]
	expr = []
	for s in ar:
		try:
			expr.append(sympy.sympify(s, evaluate=False).subs(sympy.sqrt(-1), sympy.symbols("I")))
			#expr.append(non_commutative_sympify(s).subs(sympy.sqrt(-1), sympy.symbols("I")))
		except Exception as ex:
			print(ex)
			print("Sympy is a fucking piece of shit that is incapable of converting the following string to a sympy expression:")
			print(s)
			exit(1)
	if len(expr) == 1:
		expr = expr[0]
	elif len(expr) == 2:
		expr = sympy.relational.Eq(expr[0], expr[1], evaluate=False)
	else:
		raise ValueError("strToSympy does not support multiple = signs")
	#expr = sympy.parsing.sympy_parser.parse_expr(expr, evaluate=False, transformations=(sympy.parsing.sympy_parser.standard_transformations + (sympy.parsing.sympy_parser.convert_equals_signs,)))
	#expr = expr.subs(sympy.sqrt(-1), sympy.symbols("I"))
	#expr = sympy.simplify(expr) #simplify sometimes changes lhs and rhs, e.g for mu/I*U = 1000*R_H/meter
	#expr = sympy.sympify(expr)
	for u in good_known_units_long:
		expr = expr.subs(symbols(u), good_known_units_long[u])
	return expr

def updateChash(chash, val):#TODO: Add more types
	if isinstance(val, str):
		chash.update(val.encode("utf-8"))
	elif isinstance(val, dict):
		for k in val:
			updateChash(chash, k)
			updateChash(chash, val[k])
	elif isinstance(val, (list, tuple)):
		for el in val:
			updateChash(chash, el)
	elif isinstance(val, (float, int, bool)): #TODO I don't think that this is the correct way to do it
		chash.update(str(val).encode("utf-8"))
	else:
		raise TypeError("We cannot hash a value of type " + str(type(val)))
#@endcond INTERNAL

##Assume that every symbol in expr is real
def assumeReal(expr):
	for sym in expr.free_symbols:
		expr = expr.subs(sym, symbols(str(sym), real=True))
	return expr