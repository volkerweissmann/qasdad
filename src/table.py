#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

##Uses the tex(...) command to show table as a a nicely formatted latex tabular
#\param table Accepts either a list, a dict or a table as an argument. (Note: The keys of the dictionary don't matter, the memName's do.)
#\param format Decides how the values should be printed. This argument should be a dict with the types str:ValueFormat
def showAsTabular(table, format=None):
	if isinstance(table, Table):
		table = list(table.cols.values())
	elif isinstance(table, dict):
		table = list(table.values())
	elif isinstance(table, list):
		pass
	else:
		print("Bad argument for showAsTabular: table is not of 'Table' or 'dict' or 'list' Type")
		exit(1)
	if format is not None and not isinstance(format, dict):
		print("Bad argument for showAsTabular: format is neither None nor a 'dict'")
		exit(1)
	if ValueFormat.listLength % 2 == 0:
		rowset = "lr"*((ValueFormat.listLength)//2)
	else:
		rowset = "lr"*((ValueFormat.listLength)//2)+"l"
	tex("\\begin{tabular}{" + (rowset+"|")*(len(table)-1)  + rowset + "}\n")
	begin = True
	for c in table:
		if begin:
			begin = False
			tex("\\multicolumn{" + str(ValueFormat.listLength) + "}{c}{")
		else:
			tex("&\\multicolumn{" + str(ValueFormat.listLength) + "}{|c}{")
		tex("$" + c.name.getEqName() + "$") #TODO: Dokumentation anpassen
		if c.basicSIUnit != 1:
			tex(" [$" + c.prefUnitTex  + "$]") #alt: anythingExceptNumbersToTex(c.basicSIUnit)
		tex("}")
		#texfile.write("&"*(ValueFormat.listLength-1))
	tex("\\\\\\hline\n")
	maxLength = 0
	for c in table:
		if len(c.rawData) > maxLength:
			maxLength = len(c.rawData)
	for i in range(0,maxLength):
		begin = True
		for c in range(0,len(table)):
			if begin:
				begin = False
			else:
				tex("&")
			if len(table[c].rawData) > i:
				if format is not None and table[c].name.mem in format:
					fmt = format[table[c].name.mem]
				else:
					fmt = ValueFormat(0,False)
				if math.isnan(table[c].rawData[i]):
					for i2 in range(1,ValueFormat.listLength):
						tex("&\\hspace{-1em}")
				else:
					if table[c].isDeltaZero():
						ar = fmt.toNiceTexList(table[c].rawData[i]/table[c].prefUnitFac)
					else:
						ar = fmt.toNiceTexList(table[c].rawData[i]/table[c].prefUnitFac, table[c].rawDelta[i]/table[c].prefUnitFac)
					tex("$" + ar[0] + "$")
					for i2 in range(1,len(ar)):
						tex("&\\hspace{-1em}\\hspace{-1pt}" + ar[i2])
				#texfile.write("$" + fmt.toNiceTexString(withoutUnit(table[c].data[i]), withoutUnit(table[c].delta[i])) + "$")
				#texfile.write("$" + niceNumberPrintDelta( withoutUnit(c.data[i]), withoutUnit(c.delta[i]) ) + "$")
			else:
				tex("&"*(ValueFormat.listLength-1))
		tex("\\\\\n")
	tex("\\end{tabular}\\\\")


#TODO Table should inherit from Dict, simelar to this:
# class WriteOnceDict(dict):
#     def __setitem__(self, key, value):
#         if key in self:
#             raise KeyError('{} has already been set'.format(key))
#         super(WriteOnceDict, self).__setitem__(key, value)

##Table is a super thin wrapper around python's dict that ensures that self.cols[key].name.mem == key
class Table:
        #! [table init]
	##\snippet this table init
	def __init__(self, copy=None):
		if copy is None:
			self.cols = {}
		elif isinstance(copy, Table):
			self.cols = copy.cols #shallow copy
			#print("=====================")
			#print(copy.cols)
			#self = copy #TODO #pythonlernen wieso geht das nicht
			#print(self.cols)
			#print("=====================")
		elif isinstance(copy, list):
			self.cols = {} #shallow copy
			for i in copy:
				self.cols[i.name.mem] = i
		elif isinstance(copy, dict):
			self.cols = copy #shallow copy
		else:
			print("bad arguments for Table.init")
			exit(1) #TODO raise wäre besser
	#! [table init]
	#! [table getitem]
	##\snippet this table getitem
	def __getitem__(self, key):
		return self.cols[key]
	#! [table getitem]
	#! [table setitem]
	##\snippet this table setitem
	def __setitem__(self, key, value):
		if value.name.mem != key:
			print("key and memName do not match")
			exit(1)
		self.cols[key] = value
	#! [table setitem]
	#! [table add]
	##\snippet this table add
	def add(self, value):#TODO: should we call this method "add" or "set"?
		if isinstance(value, Column):
			self.cols[value.name.mem] = value
			return
		elif isinstance(value, Table):
			value = list(value.cols.values())
		elif isinstance(value, dict):
			value = list(value.values())
		elif isinstance(value, list):
			pass
		else:
			print("Bad argument type for Table.add")
			exit(1)#TODO: raise would be better than exit, is there a "wrongTypeException" in python?
		for c in value:
			self.cols[c.name.mem] = c.deepcopy() #TODO: replace c.deepcopy() with copy.deepcopy(c) ?
	#! [table add]
	#@cond INTERNAL
	def checkGetLen(self):#TODO diese Funktion dokumentieren
		length = 1
		for mem in self.cols:
			if length == 1:
				length = len(self.cols[mem].rawData)
			elif length != len(self.cols[mem].rawData):
				raise ValueError("checkGetLen failed, because " + str(mem) + " does not have the length " + str(length))
		return length
	##Checks whether this Table is badly formatted. If so, raise
	def raiseIfBad(self):
		assert(isinstance(self.cols, dict))
		for a in self.cols:
			assert(isinstance(a, str))
			assert(isinstance(self.cols[a], Column))
			assert(self.cols[a].name.mem == a)
			self.cols[a].raiseIfBad()
		pass
	#@endcond INTERNAL
	##Removes every row fulfills the condition
	def removeRows(self, condition, test=False): #TODO: Document this function #TODO mit map kann man das besser schreiben
		#TODO should this funciton use units? it should give nice error messages if called with units
		#TODO condition = "-1*v > 19.3" and condition = "-v > 19.3" should work
		startTime = millis()
		condition = strToSympy(condition)
		condition = subsSIUnitsOne(condition)
		all_symbols = []
		for sym in condition.free_symbols:
			all_symbols.append(sym)
		numpy_condition = sympy.lambdify(all_symbols, condition, "numpy")
		arguments = []
		length = None
		for sym in all_symbols:
			if length is None:
				length = len(self.cols[str(sym)].rawData)
			elif length != len(self.cols[str(sym)].rawData):
				raise ValueErorr("differents length in table")
			arguments.append(self.cols[str(sym)].rawData)
		ar = numpy_condition(arguments)
		if isinstance(ar, bool):
			ar = np.full_like((length), ar)
		else:
			ar = ar[0]
		bools = np.logical_not(ar)
		for mem in self.cols:
			self.cols[mem].rawData = self.cols[mem].rawData[bools]
			if self.cols[mem].rawDelta is not None:
			    self.cols[mem].rawDelta = self.cols[mem].rawDelta[bools]
		# rowNum = 0
		# while rowNum < self.checkGetLen():
		# 	cond = condition
		# 	for mem in self.cols:
		# 		cond = cond.subs(mem, self.cols[mem].rawData[rowNum]) #*self.cols[mem].basicSIUnit
		# 	print(cond)
		# 	if cond:
		# 		for mem in self.cols:
		# 			self.cols[mem].rawData = np.delete(self.cols[mem].rawData, rowNum)
		# 	else:
		# 		rowNum += 1
		
	##Removes every row, where one of the selected columns is not a number
	#\param memTuple Tuple or array of memNames of the selected columns
	def removeNanRows(self, memTuple):
		bools = []
		for i in range(0, self.checkGetLen()):
			flag = True
			for mem in memTuple:
				if isnan(self.cols[mem].rawData[i]):
					flag = False
			bools.append(flag)
		for mem in self.cols:
			self.cols[mem].rawData = self.cols[mem].rawData[bools]
			if self.cols[mem].rawDelta is not None:
			    self.cols[mem].rawDelta = self.cols[mem].rawDelta[bools]
	##Only Used for debug purposes, exact format is not guaranteed to be stable
	def __str__(self):
		return "table:" + repr(self.cols)
	##Only Used for debug purposes, exact format is not guaranteed to be stable
	def __repr__(self):
		return str(self)
