#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

decimalSeperator = "{.}"
##Sets the decimal seperator that is used in latex code
def setDecimalSeperator(sep):
	global decimalSeperator
	decimalSeperator = sep

pdflang = "en"
##Sets the language used in latex output.
def setPdfLang(lang):
	assert(lang == "en" or lang == "de")
	global pdflang
	pdflang = lang
def getPdfLang():
	return pdflang

# class myq(AtomicExpr):
# 	def __init__(self, raw, unit):
# 		self.raw = raw
# 		self.unit = unit
# 	def _latex(self, printer):
# 		unit = sympy.latex(self.unit)
# 		if unit == "1":
# 			return str(self.raw)
# 		for u in good_known_units_short:
# 			unit = unit.replace(str(good_known_units_short[u]), "\\mathrm{" + u + "}")
# 		return str(self.raw) + r"\," + unit

#@cond INTERNAL
##Rounds all number in a and converts them to scientific notation
#Special 1: If expr has a "pi" in it, the return value will have a pi in it and not a 3.14.
def roundSympy(a):
	for typ in typeFunctions:
		if isinstance(a, typ):
			newargs = []
			for g in a.args:
				newargs.append(roundSympy(g))
			return typ(*tuple(newargs), evaluate=False)
			#return typ(newargs[0], tuple(newargs[1:]), evaluate=False)
	if isinstance(a, sympy.physics.units.quantities.Quantity):
		return a
	if isinstance(a, sympy.symbol.Symbol):
		return a
	if isinstance(a, sympy.UnevaluatedExpr):
		return roundSympy(a.args[0])
	if isinstance(a, (int, sympy.numbers.Integer)):
		if  abs(a) < 10000:
			return a
	if isinstance(a, specialSympyNumberTypes): #Needed because of special 1
		return a
	if isinstance(a, sympy.numbers.Rational):
		if abs(a.p) < 100 and abs(a.q) < 100:
			return a
	if isinstance(a, sympy.numbers.NaN):
		raise ValueError("You cannot round NaN")
	try:
		a = float(a)
		expValue = math.floor(math.log(abs(a), 10))
		return sympy.numbers.Float(a, 4)
		#expValue = math.floor(math.log(abs(a), 10))
		#return sympy.numbers.Float(round(a/10**expValue, 3)*10**expValue)
		#return sympy.Mul(round(a/10**expValue, 3), sympy.Pow(10, expValue, evaluate=False), evaluate=False)
	except:
		pass
	print("############")
	print(a.args)
	print(str(type(a)))
	print(repr(type(a)))
	raise NotImplementedError("unsupported operation for roundSympy:", type(a))

##converts units and sympy expressions to nice formattet latex code. For Numbers use niceNumberPrintDigits and niceNumberPrintDelta instead
#Special 1: If expr has a "pi" in it, the return value will have a \pi in it and not a 3.14.
def anythingExceptNumbersToTex(expr, contextWithspace=False):#https://stackoverflow.com/questions/23824687/text-does-not-work-in-a-matplotlib-label #TODO: Das contextWithspace ist eigentlich nur als temporärer fix dedact. Wo braucht man es und wo nicht?
	if isinstance(expr, str):
		expr = strToSympy(expr)
	expr = roundSympy(expr)
	#unit = sympy.latex(unit).replace(" ", "cdot ")
	#fold_func_brackets=True
	expr = prettyLatex(expr, fold_short_frac=True) \
		    .replace(".", decimalSeperator) \
		    .replace("\\text", "\\mathrm") \
		    .replace("\\operatorname{atan}", "\\arctan") #Needed because of special 3
	for u in good_known_units_short:
		expr = expr.replace(str(good_known_units_short[u]), "\\mathrm{" + u + "}")
	if contextWithspace:
		expr = expr.replace("\\mathrm", "\\,\\mathrm")
	return expr

#e.g. aboveUnderFrac(sympy.sympify("a*b/c*d**-1")) should return [a, b],[c,d]
def aboveUnderFrac(expr):
	if isinstance(expr, sympy.physics.units.quantities.Quantity):
		return [expr], []
	elif isinstance(expr, sympy.Mul):
		ret_a = []
		ret_u = []
		for a in expr.args:
			t_a, t_u = aboveUnderFrac(a)
			ret_a += t_a
			ret_u += t_u
		return ret_a, ret_u
	elif isinstance(expr, sympy.Pow):
		if expr.args[1] >= 0: #TODO what if expr is a^b ? b>0 cant be evaluated
			return [expr], [] #TODO what if expr is (1/a)^2 
		else:
			return [], [sympy.Pow(expr.args[0], -expr.args[1])] #TODO what if expr is (1/a)^{-2}
	else:
		raise NotImplementedError("Unknown type:", type(expr))
def aboveUnderFracMul(expr):
	a,u = aboveUnderFrac(expr)
	#if len(a) == 1:
	#	a = a[0]
	#else:
	#	a = sympy.Mul(*a, evaluate=False)
	#if len(u) == 1:
	#	u = u[0]
	#else:
	#	u = sympy.Mul(*u, evaluate=False)
	return sympy.Mul(*a), sympy.Mul(*u)
siprefixdict = {15: "P", 12: "T", 9: "G", 6: "M", 3: "k", -3: "m", -6:"\\mu ", -9:"n", -12:"p", -15:"f"}
def attemptAddPrefix(mullist, pot):
	if pot == 0:
		return True
	if not pot in siprefixdict:
		raise NotImplementedError("unknown siprefixpot:", pot)
	for i in range(0, len(mullist)):
		if mullist[i] in [meter, second, hertz, coulomb, volt, ampere, henry, kelvin, newton, joule, siemens, farad, watt, tesla]:
			mullist[i] = "\\mathrm {"+ siprefixdict[pot] + sympy.latex(mullist[i])[5:] + "} "
			return True
		elif mullist[i] == ohm:
			mullist[i] = "\\mathrm {" + siprefixdict[pot] + "}\\Omega"
			return True
			#mullist[i] = "\\Omega"
	return False
##converts unit to nice text. e.g. unit=second, potdivt = 0 results in "s", unit=second, potdivt=-3 result in "ms". If inline is false it will use \frac, otherwise it will us e.g. s^{-1}. Needs a math mode.
def unitToTex(unit, pot=0, inline=False):
	assert(pot % 3 == 0)
	above, under = aboveUnderFrac(unit)
	if attemptAddPrefix(above, pot):
		pot = 0
	if attemptAddPrefix(under, -pot):
		pot = 0
	if len(above) == 0 and len(under) == 0:
		return ""
	top = ""
	for e in above:
		if isinstance(e, str):
			top += e
		else:
			top += sympy.latex(e).replace("\\text", "\\mathrm") + " "
	if len(above) == 0:
		top = "1"
	if len(under) == 0:
		return top
	bottom = ""
	for e in under:
		bottom += sympy.latex(e).replace("\\text", "\\mathrm")
	return "\\frac{" + top + "}{" + bottom + "}"
	
	ret += "}{"
	#return "\\frac{" + sympy.latex(above).replace("\\text", "\\mathrm") + "}{" + sympy.latex(under).replace("\\text", "\\mathrm") + "}"

##Returns expr without the its. Note that it does not care whether the unit is a basic SI unit or not, use subsSIUnitsOne instead if you want to compare values. Note: It's probably super buggy and will (hopefully) be rewritten soon.
def withoutUnit(expr): #source: https://stackoverflow.com/questions/51119918/how-to-concisely-get-the-units-of-a-sympy-expression?rq=1
	try:
		float(expr)
		return expr
	except:
		pass
	#if isinstance(expr.evalf(), sympy.numbers.Float):
	#	return expr
	expr = 1.0*expr
	return float(expr.subs({x: 1 for x in expr.args if x.has(sympy.physics.units.Quantity)}))

def unitTest_withoutUnit():
	assert(withoutUnit(sympy.sympify("1/(4*volt+3*volt)"))==1/7)

def betterLog(val):
	#len(str(abs(numDelta)).split(".")[0])-1
	return math.floor(1e-8+math.log(abs(val), 10))
#@endcond INTERNAL
	
##NumberFormat is a class that holds a specific way to print a value, eg Scientific Notation with 4 valid Digits and no uncertainty\n
#self.numDigits stores the number of valid digits that will be printed. If numDigits is 0 the number of digits that will be printed is inferred by the uncertainty of that value\n
#self.showDelta stores whether the uncertainty should be printed
class ValueFormat: #Why the f#@* does python not have real rust-like enums?
	#! [ValueFormat init]
	##\param numDigits stores the number of valid digits that will be printed. If numDigits is 0 the number of digits that will be printed is inferred by the uncertainty of that value\n
	#\param showDelta stores whether the uncertainty should be printed
	#\param mode "scientific": Use the scientific notation. "classic": Do not use the scientfic notation, "default": Uses sometimes the scientific notation and sometimes classic notation
	#\snippet this ValueFormat init
	def __init__(self, numDigits, showDelta, mode="default"):
		self.numDigits = numDigits
		self.showDelta = showDelta
		self.mode = mode
	#! [ValueFormat init]
	##Returns the value "value" as a nice latex string. Acts if showDelta is False if delta is NaN. Crashes if value is NaN.
	def toNiceTexString(self, value, delta=None):
		ar = self.toNiceTexList(value, delta)
		ret = ""
		for s in ar: #TODO is there a more pyhtonic way to do that
			ret += s
		return ret
	#@cond INTERNAL
	##Length of the list returned by toNiceTexList
	listLength = 4 #TODO is it possible in python to declare this as static or constant
	##Does not work if numValue is zero. shownDigitsLeadZero is the number of digits of numVale that should be shown, excluding leading zeros
	def format(self, value, numValue, numDelta, showDelta, cdotexp, shownDigits):
		ret = ["", "", "\\ensuremath{", "\\ensuremath{"]
		if numValue < 0:
			ret[0] = "-"

		vallog = betterLog(numValue)
		digitsAfterPoint = shownDigits - vallog + cdotexp -1
		#ret[1] +=  (("%." + str(digitsAfterPoint) + "f") % (abs(numValue)/10**cdotexp) ).replace(".", decimalSeperator)
		numstr =  (("%." + str(digitsAfterPoint) + "f") % (abs(numValue)/10**cdotexp) ).split(".")
		ret[1] = "\\ensuremath{" + numstr[0] + "}"
		if len(numstr) > 1:
			ret[2] += decimalSeperator + numstr[1]
		explastdigit = cdotexp-digitsAfterPoint
		if showDelta:
			ret[2] += "(" + str(int(math.ceil(  numDelta/10**explastdigit  ))) + ")"
		if cdotexp != 0:
			ret[3] += "\\cdot 10^{" + str(cdotexp) + "}"

		if checkGetUnit(value) != 1:
			ret[3] += r"\,\," + anythingExceptNumbersToTex(checkGetUnit(value))
		ret[2] += "}"
		ret[3] += "}"
		return ret
	def handleZero(self, delta, numDelta, showDelta):
		if numDelta == None:
			return ["", "0", "", ""]
		ret = ["", "\\ensuremath{", "", "\\ensuremath{"]
		ret[1] += "0"
		deltalog = betterLog(numDelta)
		if showDelta:
			ret[1] += "(" + str(int(math.ceil(  numDelta/10**deltalog  ))) + ")"
		if deltalog != 0:
			ret[3] += "\\cdot 10^{" + str(deltalog) + "}"
		if checkGetUnit(delta) != 1:
			ret[3] += r"\,\," + anythingExceptNumbersToTex(checkGetUnit(delta))
		ret[1] += "}"
		ret[3] += "}"	
		return ret
	##Similar to toNiceTexString, but splits the result into a list of the length "ValueFormat.listLength". E.g. ValueFormat(0,True).toNiceTexList(213,2) = ['2,130(2)', '\\cdot 10^{2}']
	def toNiceTexList(self, value, delta=None): #TODO ValueFormat(5, True).toNiceTexString(1.2345, 0.1) zeigt einen falschen fehler an.
		if isnan(value):
			raise ValueError("value is NaN.")
		if isinstance(value, np.float) and (value == np.inf or value == -np.inf):
			raise ValueError("value is inf")
		if isinstance(delta, np.float) and (delta == np.inf or delta == -np.inf):
			raise ValueError("delta is inf")
		numValue = withoutUnit(value)
		if delta is None or isnan(delta):
			numDelta = None
		else:
			numDelta = withoutUnit(delta) #PERFORMANCE: If numDigits is not 0 and showDelta is False, we dont need this line)		
		if numDelta is not None  and numDelta <= 0:
			raise ValueError("delta <= 0: "+str(numDelta))
		if numDelta is None and self.showDelta == True:
			raise ValueError("Bad arguments for toNiceTexList: No delta is given, but you want us to show delta.")
		if numDelta is None and self.numDigits == 0:
			raise ValueError("Bad arguments for toNiceTexList: No delta is given, but you espect us to round based on delta")
		#numValue /= self.prefUnitFac
		#numDelta /= self.prefUnitFac
		if numValue != 0:
			vallog = betterLog(numValue)
		else:
			return self.handleZero(delta, numDelta, self.showDelta)
			vallog = None
		if numDelta is not None:
			deltalog = betterLog(numDelta)
		else:
			deltalog = None
			
		if self.numDigits != 0:
			shownDigits = self.numDigits
		else:
			shownDigits = max(1, 1+vallog-deltalog)
		assert(self.mode in ["default", "scientific", "simple"])
		if self.mode == "simple":
			return self.format(value, numValue, numDelta, self.showDelta, 0, max(shownDigits,vallog+1))
		elif self.mode != "scientific" and vallog >= -3 and vallog <= 4 and shownDigits-vallog-1 >= 0: #The check if shownDigit-vallog-1 >= 0 has to be there, e.g for the case numValue = 123 and shownDigits = 2
			return self.format(value, numValue, numDelta, self.showDelta, 0, shownDigits)
		else:
			return self.format(value, numValue, numDelta, self.showDelta, vallog, shownDigits)
	#@endcond INTERNAL