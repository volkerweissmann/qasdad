#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

#@cond INTERNAL
##Checks whether the argument is a well-formatted dictionary of well-formatted Columns. raises otherwise
def debugCheckColumnTable(colt):
	if not isinstance(colt, dict):
		raise TypeError("colt is of type:", type(colt))
	for a in colt:
		if not isinstance(colt[a], Column):
			raise TypeError("a member of collist is of type:", type(colt[a]))
		if colt[a].name.mem != a:
			raise ValueError("colt["+repr(a)+"].name.mem=", colt[a].name.mem)
		colt[a].raiseIfBad()
	
##Splits a latex string in multiple lines so that it fits on a din a4 page. E.g. latexLineSplit("a+b+c+...+z") might return "a+b+c+..+m\\n+...+z"
def latexLineSplit(lat):
	if not isinstance(lat, str):
		raise TypeError("lat in latexLineSplit should be an instance of str")
	if "\\\\" in lat:
		raise ValueError("not implemented yet")
	possibleSplits = []
	# curledBraceDepth = 0
	# waitstate = 0 #waitstate is 0 at default, in \alpha 1 , in \sqrt 2 in \frac 3 and in _ 2
	# indexstate = 0
	# for i in range(0, len(lat)): #TODO das hier mal auf performance untersuchen
	# 	print("loop begin", i, curledBraceDepth, waitstate, indexstate)
	# 	if curledBraceDepth == 0 and indexstate == 2 and lat[i] not in [" ", "\t", "\n"]:
	# 		indexstate = 1
	# 	elif indexstate == 1:
	# 		indexstate = 0
	# 	elif curledBraceDepth == 0 and lat[i] in ["_", "^"]:
	# 		indexstate = 2

	# 	if curledBraceDepth == 0 and waitstate == 0 and indexstate == 0:
	# 		possibleSplits.append(i)	
			
	# 	if curledBraceDepth == 0 and lat[i] == "\\":
	# 		if len(lat)-i >= 1+len("sqrt") and lat[1:1+len("sqrt")] == "sqrt":
	# 			waitstate = 2
	# 		elif len(lat)-i >= 1+len("frac") and lat[1:1+len("frac")] == "frac":
	# 			waitstate = 3
	# 		else:
	# 			waitstate = 1
	# 	elif curledBraceDepth == 0 and waitstate != 0 and lat[i] in [" ", "\t", "\n", "+", "-", "*", "/", "{", "}"]:
	# 		waitstate -= 1
			
	# 	if lat[i] == "{":
	# 		curledBraceDepth += 1
	# 	elif lat[i] == "}":
	# 		curledBraceDepth -= 1
	# possibleSplits.insert(0, 0)
	# possibleSplits.append(len(lat))
	# for i in range(0, len(possibleSplits)-1):
	#	print(lat[possibleSplits[i]:possibleSplits[i+1]])
	
	curledBraceDepth = 0
	for i in range(0, len(lat)):
		if curledBraceDepth == 0 and lat[i] == "+" or lat[i] == "=": #TODO es gibt noch viel mehr Fälle in denen das geht, wenn ich wach bin dass mal ordentlich machen
			possibleSplits.append(i)
		if lat[i] == "{":
			curledBraceDepth += 1
		elif lat[i] == "}":
			curledBraceDepth -= 1
			
	splits = [0]
	#for i in possibleSplits:
	#	if lat[i] == "=":
	#		splits.append(i)
	#	elif i-splits[-1] > 80:
	#		splits.append(i)
	possibleSplits.append(len(lat))
	for i in range(0, len(possibleSplits)-1):
		if lat[possibleSplits[i]] == "=":
			splits.append(possibleSplits[i])
		elif possibleSplits[i+1]-splits[-1] > 300:
			splits.append(possibleSplits[i])

	ret = ""
	for i in range(0, len(splits)-1):
		ret += lat[splits[i]:splits[i+1]] + "\\\\"
	ret += lat[splits[-1]:]
	return ret

##Prints every part of a sympy expression. Only for debug purposes
def debugPrintExprArgs(expr, indent=0):
	if len(expr.args) == 0:
		print("\t"*indent + str(type(expr)) +repr(expr))
	else:
		print("\t"*indent + str(type(expr)) + ":")
		for a in expr.args:
			debugPrintExprArgs(a, indent + 1)

#@endcond INTERNAL

##Reads a file and returns a table
#File Format: e.g.\n
#\#t ; x1 eq:x_1 ; x2 eq:x_2 \n
#\#s ; 0.0254*m ; 10**3*m\n
#10,35 1.00 0.796\n
#35.10 900 4,3\n
#The first line contains the names of the Coloumns: We use Name.parseString for parsing the string between the semicolons.\n
#The second line gets multiplied with every line. This is useful for declaring units and 10^n's. For a total list of supported units look at good_known_units_short. SI unit prefixes are not supported. In this example, the first column would be in seconds, the second column in inch and the third column in kilometers.\n
#From the third line onwards, both "," and "." have the same meaning, the decimal seperator.\n
#Security notice: readFile uses sympy.sympyfy which uses eval() which should not be used on untrusted input. Therefore, readFile(filepath) should only be run if the content of filepath is at least as trustworthy as this script.
##\param names: You can leave out the first line in the file if you pass this optional argument. It has to be a list of Name-objects. The length of the list has to be the number of Columns in the file.
##\param unitFactor: You can leave out the second line in the file if you pass this optional argument. It has to be a list of sympy expressions. The length of the list has to be the number of Columns in the file.
def readFile(filepath, names=None, unitFactor=None, format="default"):
	if names is not None:
		for name in names:
			assert(isinstance(name,Name))
	with open(filepath) as fp:
		if names is None:# T ; i1 ; i2 ; i3 ; m2576 ; i4 ; a2576 ; i5 ; m9997 ; i6 ; a9997 ; i7 ; m35543 ; i8 ; a35543 ; i9 ; m109961 ; i10 ; a109961
# kelvin ; 1 ; 1 ; 1 ; 1 ; 1 ; volt ; 1 ; 1 ; 1 ; volt ; 1 ; 1 ; 1 ; volt ; 1 ; 1 ; 1 ; volt
			names = fp.readline().split(";")
			names[0] = names[0][1:] #removes the # before the first name
			try:
				names = [Name.parseString(o) for o in names]
			except ValueError as ex:
				print(ex)
				print("The file", filepath, " has badly formatted names. (See exception above) ")
				exit(1)
		if unitFactor is None:
			unitFactor = fp.readline().split(";")
			unitFactor[0] = unitFactor[0][1:]
		units = [sympy.sympify(o).subs(good_known_units_long) for o in unitFactor]
		factors = [subsSIUnitsOne(u) for u in units]
		for i in range(0, len(units)):
			units[i] = units[i]/factors[i]
		factors = [float(f) for f in factors]
		ret = Table()
		filestr = ""
		with open(filepath) as fp:  
			for line in fp:
				filestr += line.replace("\t", " ").replace(",", ".")
		try:
			if format == "default":
				matrix = np.loadtxt(io.StringIO(filestr), dtype=str, ndmin=2)
			elif format == "csv":
				matrix = np.loadtxt(io.StringIO(filestr), dtype=str, ndmin=2, delimiter=';')
			else:
				raise ValueError("bad format")
		except Exception as e:
			debugfile = open("fail.txt", "w")
			debugfile.write(filestr)
			debugfile.close()
			print("Bad data in file:", filepath)
			print(e)
			exit(1)
		if len(matrix[0]) != len(names):
			print("The file", filepath, " is badly formatted:\n")
			print("It has",len(names),"names, but ",len(matrix[0]),"columns")
			exit(1)
		if len(matrix[0]) != len(unitFactor):
			print("The file", filepath, " is badly formatted:\n")
			print("It has",len(unitFactor),"unit factors, but ",len(matrix[0]),"columns")
			exit(1)
		for i in range(0,len(matrix[0])):
			dat = []
			for o in matrix[:,i]:
				if o == "":
					dat.append(np.nan)
				else:
					dat.append(float(o))
			rawDelta = []
			for o in matrix[:,i]:
				ar = o.split(".")
				digits = 0
				if len(ar) == 2:
					digits = len(ar[1])
				rawDelta.append(10**(-digits))
			dat = np.array(dat)*factors[i]
			rawDelta = np.array(rawDelta)*abs(factors[i])
			ret.add(Column(names[i],units[i],dat,rawDelta=rawDelta))
	return ret

#! [showAsTable]
##\snippet this showAsTable
def showAsTable(table, label, caption, format=None):
	tex("\\begin{table}[tbp]\\centering")
	showAsTabular(table, format)
	tex("\\caption{" + caption + "}")
	tex("\\label{" + label + "}")
	tex("\\end{table}")
#! [showAsTable]

#@cond INTERNAL
def getMaxima(col):
	x = []
	y = []
	for i in range(1, len(col.rawData)-1):
		if col.rawData[i] > col.rawData[i-1] and col.rawData[i] > col.rawData[i+1]:
			x.append(i)
			y.append(col.rawData[i])
	return x,y
def testGetMaxima(path, x, y):
	table = readFile(path)
	p = Plot(display=True)
	p.add("", table[x], table[y], xerr=False, yerr=False)
	a,b = getMaxima(table[y])
	for i in range(0, len(a)):
		a[i] = table[x].rawData[a[i]]
	p.ax.plot(a, b, linestyle="", marker="+")
	#p.showHere()
	matplotlib.pyplot.show()
#@endcond INTERNAL
