#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

greektex = ["alpha", "beta", "gamma","Gamma", "delta","Delta","epsilon","zeta","eta","theta","Theta","iota","kappa","lambda","Lambda","mu","nu","omicron","pi","Pi","rho","sigma","Sigma","tau","upsilon","Upsilon","phi","Phi","chi","psi","Psi"]
##Holds all names, abbreviatians and latex expressions of a Column
class Name:
	##Name used for internal use, including sympy expressions
	mem = None
	##Name used in equations. May be None
	eqName = None

	##Name used in equations. Will never return None
	def getEqName(self):
		if self.eqName is not None:
			return self.eqName.replace(" ","{}") #Without this, special 4 fails
		ret = self.mem
		for gt in greektex:
			ret = replaceTexSymbol(ret, gt, "\\"+gt, True)
		return ret

	##Constructor
	#\param memName: Used in python strings
	#\param eqName: Used in latex. If eqName=None, we will use memName in latex.
	def __init__(self, memName, eqName=None):
		if not isinstance(memName, str):
			raise TypeError("bad Type: " + str(type(memName)))
		if memName == "":
			raise ValueError("memName is an empty string")
		if " " in memName or "\t" in memName or "\n" in memName:
			raise ValueError("You cannot have a memName with space in it")
		self.mem = memName
		self.eqName = eqName

	##Parses a string like " deltat " or " deltat eq:\\Delta t " to a Name object. The first word is the "memName" an it's used to refer to the Column in python. The word after eq: is the latex code used when displaying it. If you leave out the eq:... part, we will just use the memName in latex. This function is used in readFile
	@staticmethod
	def parseString(str):
		str = str.lstrip().rstrip()
		p = str.find(":")
		if p == -1:
			return Name(str)
		if str[p+1:].find(":") != -1:
			raise ValueError("You cannot have multiple colons in str")
		if str[p-len("eq"):p] != "eq":
			raise ValueError("Bad namestring: " + str + "\nA colon is only allowed in eq:")
		if not str[p-1-len("eq")].isspace():
			raise ValueError("You need a space before eq:")
		ret = Name(str[:p-len("eq")].rstrip(), str[p+1:].lstrip())
		return ret
	#@cond INTERNAL
	def raiseIfBad(self):
		if self.mem is None:
			raise ValueError("mem is None")
	#@endcond INTERNAL
	def __repr__(self):
		if self.eqName is None:
			return "Name(\"" + pyescape(self.mem) + "\")"
		else:
			return "Name(\"" + pyescape(self.mem) + "\", eqName=\"" + pyescape(self.eqName) + "\")"
