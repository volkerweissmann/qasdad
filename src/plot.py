#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

#@cond INTERNAL
DATA_PATH = None
def setDataPath(datapath):
	global DATA_PATH
	DATA_PATH = datapath
LATEX_PATH = None
def setLatexPath(latexpath):
	global LATEX_PATH
	LATEX_PATH = latexpath
def setNoTexPlot(noTexPlot): #TODO: should noTexPlot influence chash
	matplotlib.rc("text", usetex=(not noTexPlot))
CACHE_PATH = None
def setCachePath(arg):
	global CACHE_PATH
	CACHE_PATH = arg
	readCache()
	
plotCacheDict = {}
def readCache():
	global plotCacheDict
	try:
		plotCacheDict = pickle.load(open(os.path.join(CACHE_PATH, "pickle"), "rb"))
	except FileNotFoundError:
		pass
def saveCache():
	if CACHE_PATH is None:
		print("CACHE_PATH is None, we won't save the cache")
		return
	try:
		os.makedirs(CACHE_PATH)
	except FileExistsError:
		pass
	pickle.dump(plotCacheDict, open(os.path.join(CACHE_PATH, "pickle"), "wb"))
atexit.register(saveCache)

def relsymlink(origpath, linkpath, dirorfile):
	if os.name == "nt":
		if dirorfile == "dir":
			#os.system('mklink /J "' + linkpath + '" "' + origpath + '"')
			shutil.copytree(origpath, linkpath)
		elif dirorfile == "file":
			#os.system('mklink /H "' + linkpath + '" "' + origpath + '"')
			os.link(origpath, linkpath)
		else:
			raise ValueError("dirorfile is neither dir nor file")
	else:
		os.symlink(os.path.relpath(origpath,start=os.path.dirname(linkpath)), linkpath)

##No one is allowed to write into a filepath that is in this array
reservedFiles = []

#@endcond INTERNAL

##Holds everything necessary to describe one diagramm
class Plot:
	## Constructor
	#\param size The size of the figure
	def __init__(self, size=(7,5), display=False, logPath=None): #TODO den display Parameter und den logPath Parameter dokumentieren, matplotlib.pyplot.show()
		self.xlabel = None
		self.ylabel = None
		if logPath is not None:
			self.logFile = open(logPath, "w")
		else:
			self.logFile = None
		if not self.logFile is None:
			self.logFile.write("import matplotlib\n")
			self.logFile.write("import matplotlib.pyplot\n")
		#matplotlib.rcParams['text.latex.unicode'] = True
		#self.logFile.write("matplotlib.rcParams['text.latex.unicode'] = True\n")
		self.lines = {}
		if display:
			self.fig = matplotlib.pyplot.figure(constrained_layout=True) #PERFORMANCE: Remove constrained_layout ?
			if not self.logFile is None:
				self.logFile.write("fig = matplotlib.pyplot.figure(constrained_layout=True)\n")
		else:
			self.fig = matplotlib.figure.Figure(constrained_layout=True, figsize=size, dpi=1000)
			matplotlib.backends.backend_agg.FigureCanvasAgg(self.fig) #I'm not sure, but this is useless
			if not self.logFile is None:
				self.logFile.write("fig = matplotlib.pyplot.figure(constrained_layout=True, figsize=" + str(size) + ", dpi=1000)\n")
				self.logFile.write("matplotlib.backends.backend_agg.FigureCanvasAgg(fig)\n")
		self._ax = self.fig.add_subplot(111) #alternative: .suplot( instead of .add_suplot(
		self._ax.ticklabel_format(style='sci',scilimits=(-3,4),axis='both')
		if not self.logFile is None:
			self.logFile.write("ax = fig.add_subplot(111)\n")
		#https://matplotlib.org/examples/ticks_and_spines/tick-locators.html
		#https://github.com/matplotlib/matplotlib/issues/8768
		#https://github.com/matplotlib/matplotlib/pull/12865
		self.chash = hashlib.md5()
		updateChash(self.chash, size)
		self.xfac = 1
		self.yfac = 1
	##\snippet this Plot add
	#! [Plot add]
	def add(self, xColumn, yColumn, lineName = None, xerr=False, yerr=False, **kwargs):
		if lineName is None:
			i = 0
			while "default"+str(i) in self.lines:
				i+=1
			lineName = "default"+str(i)
		if lineName in self.lines:
			raise ValueError("You can't have multiple PlotLines with the same lineName in the same plot")
		self.lines[lineName] = PlotLine(self.logFile, xColumn, yColumn, xerr, yerr, **kwargs)
	#! [Plot add]
	##Use this to access the axes of matplotlib directly
	def accessAxes(self, func, **kwargs):
		self.chash.update(func.encode("utf-8"))
		updateChash(self.chash, kwargs)
		cmd = "self._ax." + func
		func = eval(cmd)
		func(**kwargs)
	#@cond INTERNAL
	##Saves the plot in a file
	##\param path Filepath of the graph. Has to include a correct file extension.
	def writeToFile(self, path, **kwargs):
		if self.xlabel is not None:
			self._ax.set_xlabel(self.xlabel)
		elif len(self.lines) != 0:
			maths = []
			bsiu = None #x-axsis basic SI Unit
			tmin = np.inf
			tmax = -np.inf
			for line in self.lines.values():
				maths.append(line.xcol.name.getEqName())
				if bsiu is None:
					bsiu = line.xcol.basicSIUnit
				elif bsiu != line.xcol.basicSIUnit:
					raise IncompDimension("Attempting to plot different x-dimensions in the same plot")
				lmin = np.min(line.xcol.rawData)
				lmax = np.max(line.xcol.rawData)
				if lmin < tmin:
					tmin = lmin
				if lmax > tmax:
					tmax = lmax
			tabs = max(abs(tmin), abs(tmax))
			if maths[1:] != maths[:-1]: #if not alle list ellements are equal
				raise ValueError("You should set self.xlabel for this instance of Plot.")
			if bsiu == 1: #TODO PERFORMANCE: if bsiu == 1 we dont need to calculate tmin, tmax an tabs
				self._ax.set_xlabel("$" + maths[0] + "$")
			else:
				presi = ""
				#if tabs < 0.1 and tabs > 0.1*10**-3:
				#	presi = "\\mathrm{m}"
				#	self.xfac = 10**-3
				self._ax.set_xlabel("$" + maths[0] + "$ [$" + presi + anythingExceptNumbersToTex(bsiu) + "$]")
		if self.ylabel is not None:
			self._ax.set_ylabel(self.ylabel)
		elif len(self.lines) != 0:
			maths = []
			bsiu = None #y-axsis basic SI Unit
			tmin = np.inf
			tmax = -np.inf
			for line in self.lines.values():
				maths.append(line.ycol.name.getEqName())
				if bsiu is None:
					bsiu = line.ycol.basicSIUnit
				elif bsiu != line.ycol.basicSIUnit:
					raise IncompDimension("Attempting to plot different y-dimensions in the same plot")
				lmin = np.min(line.ycol.rawData)
				lmax = np.max(line.ycol.rawData)
				if lmin < tmin:
					tmin = lmin
				if lmax > tmax:
					tmax = lmax
			tabs = max(abs(tmin), abs(tmax))
			if maths[1:] != maths[:-1]: #if not all list ellements are equal
				raise ValueError("You should set self.ylabel for this instance of Plot.")
			mathname = maths[-1]
			if bsiu == 1: #TODO PERFORMANCE: if bsiu == 1 we dont need to calculate tmin, tmax an tabs
				self._ax.set_ylabel("$" + mathname + "$")
			else:
				presi = ""
				#if tabs < 0.1 and tabs > 0.1*10**-3:
				#	presi = "\\mathrm{m}"
				#	self.yfac = 10**-3
				self._ax.set_ylabel("$" + mathname + "$ [$" + presi + anythingExceptNumbersToTex(bsiu) + "$]")
		self.chash.update(self._ax.get_xlabel().encode("utf-8"))
		self.chash.update(self._ax.get_ylabel().encode("utf-8"))
		self.chash.update(self._ax.get_title().encode("utf-8"))
		for key in self.lines:
			self.lines[key].updateChash(self.chash)
		self.chash = self.chash.digest()
		print(path, self.chash)
		if self.chash in plotCacheDict:
			print("link", plotCacheDict[self.chash], path)
			relsymlink(plotCacheDict[self.chash], path, "file")
			return
		#self._ax.xaxis.set_minor_locator(matplotlib.ticker.NullLocator())
		#self._ax.yaxis.set_minor_locator(matplotlib.ticker.NullLocator())
		#self._ax.xaxis.set_major_locator(matplotlib.ticker.AutoLocator())
		#self._ax.yaxis.set_major_locator(matplotlib.ticker.AutoLocator())

		for line in self.lines.values():
			if "label" in line.kwargs:
				self._ax.errorbar(line.xcol.rawData/self.xfac, line.ycol.rawData/self.yfac, xerr=line.xdelta, yerr=line.ydelta, **line.kwargs)
			else:
				self._ax.errorbar(line.xcol.rawData/self.xfac, line.ycol.rawData/self.yfac, xerr=line.xdelta, yerr=line.ydelta, label=line.label, **line.kwargs)
			if line.fittedfunc is not None:
				startX = line.xshowfitlow
				stopX = line.xshowfithigh
				if self._ax.get_xscale() == "linear":
					xdata = np.linspace(startX,stopX, 1000)
				elif self._ax.get_xscale() == "log":
					xdata = np.logspace(np.log2(startX),np.log2(stopX), 1000, base=2)
				else:
					raise ValueError("unknown axis scale")
				ydata = line.fittedfunc(xdata)
				if not xdata.shape == ydata.shape:
					ydata = np.full(len(xdata),ydata)
				if "label" in line.fitkwargs:
					self._ax.plot(xdata/self.xfac, ydata/self.yfac, zorder=100, **line.fitkwargs)
				else:
					self._ax.plot(xdata/self.xfac, ydata/self.yfac, label=line.fitlabel, zorder=100, **line.fitkwargs)
		self._ax.legend(**kwargs)
		if not self.logFile is None:
			self.logFile.write("fig.savefig('" + path + "')")

		for i in range(0,1000):
			plot_path = os.path.join(CACHE_PATH, str(i) + ".pdf") #.png does not work on my laptop
			#plotCacheDict
			incachedict = plot_path in plotCacheDict.values()
			fileexists = os.path.exists(plot_path)
			if incachedict != fileexists:
				print("WARNING: "+"\""+plot_path+"\" in plotCacheDict.values() returns", incachedict, "but os.path.exists(\""+plot_path+"\") returns", fileexists)
			if not incachedict: #not os.path.exists(plot_path):
				plotCacheDict[self.chash] = plot_path
				try:
					os.makedirs(CACHE_PATH)
				except FileExistsError:
					pass
				self.fig.savefig(plot_path)
				relsymlink(plot_path, path, "file")
				print("regend", plot_path, path)
				return
		print("unable to safe plot because the chache has exceeded 1000 plots. Try deleting the cache.")
		exit(1)
	#@endcond INTERNAL
	##Saves the plot as a png and runs tex("\includegraphics[width=size\\linewidth]{path}"). **kwargs are passed to self._ax.legend
	def showHere(self,size=1.0, **kwargs):
		for i in range(0,100):
			plot_path = os.path.join(DATA_PATH, str(i) + ".pdf") #.png does not work on my laptop
			if not plot_path in reservedFiles: #not os.path.exists(plot_path):
				reservedFiles.append(plot_path)
				self.writeToFile(plot_path)
				rawpath = os.path.relpath(plot_path,start=os.path.dirname(LATEX_PATH))
				if os.name == "nt":
					tex("\\includegraphics[width=" + str(size) + "\\linewidth]{" + rawpath.replace("\\","/")  +"}\\\\" ) #TODO it is better to use latex relative path import here. #TODO check what special characters (\ and #) there are in LaTex
				else:
					assert("\\" not in rawpath)
					tex("\\includegraphics[width=" + str(size) + "\\linewidth]{" + rawpath  +"}\\\\" )
				return
		print("unable to safe plot")
		exit(1)
	#! [Plot showAsFigure]
	##\snippet this Plot showAsFigure
	def showAsFigure(self, label, caption, size=1.0, pos="tbp", **kwargs):
		tex("\\begin{figure}["+pos+"]")
		tex("\\centering")
		self.showHere(size, **kwargs)
		tex("\\caption{" + caption + "}")
		tex("\\label{" + label + "}")
		tex("\\end{figure}")
		self.latexlabel = label
		for line in self.lines:
			self.lines[line].latexlabel = label
	#! [Plot showAsFigure]
	#! [Plot showAsFigureH]
	##\snippet this Plot showAsFigureH
	def showAsFigureH(self, label, caption, size=1.0, **kwargs):
		tex("\\begin{figure}[H]")
		tex("\\centering")
		self.showHere(size, **kwargs)
		tex("\\caption{" + caption + "}")
		tex("\\label{" + label + "}")
		tex("\\end{figure}")
		self.latexlabel = label
		for line in self.lines:
			self.lines[line].latexlabel = label
	#! [Plot showAsFigureH]
	#! [Plot getitem]
	##\snippet this Plot getitem
	def __getitem__(self, key):
		return self.lines[key]
	#! [Plot getitem]
