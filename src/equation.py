#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

##Holds an equation, consisting of a sympy equation, a latex equation and a dictionary that matches latex representations to sympy symbols
class Equation:
	#Special 1: If expr has a "pi" in it, eqLatex will have a \pi in it and not a 3.14.
	#Special 2: A nameDict key may be a string with a space, e.G. \\Delta t
	##Construct an Equation from a sympy equation and a dictionary that maps latex representations to sympy symbols.
	@staticmethod
	def fromSympy(expr, nameDict={}, simplify=False):
		if isinstance(expr, str):
			expr = strToSympy(expr)
		#else:
		#	raise TypeError("Bad argument for Equation.fromSympy")
		if simplify:
			try:
				expr = sympy.simplify(expr)
			except:
				raise ValueError("sympy.simplify failed")
		if not isinstance(nameDict, dict):
			raise TypeError("Bad type of nameDict in Equation.fromSympy")
		for k in nameDict:
			if isinstance(nameDict[k], str):
				nameDict[k] = symbols(nameDict[k])
		for s in expr.free_symbols:
			if s not in nameDict.values():
				lat = sympy.latex(s)
				if lat in nameDict:
					raise ValueError("Name clash: " + lat + " is the Latex represenation of both " + str(s) + " and " + str(nameDict[s]))
				nameDict[lat] = s
		for k in nameDict:
			if not isinstance(k, str):
				raise TypeError("Bad type in nameDict keys in Equation.formSympy")
			if not isinstance(nameDict[k], sympy.symbol.Symbol):
				raise TypeError("Bad type in nameDict values in Equation.formSympy")
		ret = Equation()
		ret.nameDict = nameDict
		#inverseDict = dict([[v,symbols(k)] for k,v in nameDict.items()]) #We have to use symbols(k) instead of k, because otherwise symbols("I") will get replaced by the imaginary unit

		#We have to use symspace(k) or symbols(k) instead of k, because otherwise symbols("I") will get replaced by the imaginary unit
		#We have to use symspace instead of symbols because of special 2
		inverseDict = dict([[v,sympy.UnevaluatedExpr(symspace(k))] for k,v in nameDict.items()])
		
		ret.eqSympy = expr
		#ret.eqLatex = sympy.latex(expr.subs(inverseDict))
		ret.eqLatex = anythingExceptNumbersToTex(expr.subs(inverseDict))
		return ret
	##Constructs an Equation from a latex equation, a dictionary that maps latex representations to sympy symbols
	@staticmethod
	def fromLatex(latex, nameDict={}):
		ret = Equation()
		ret.eqLatex = latex
		latex = latex.replace("\\right", "").replace("\\left","").replace("\\,","")
		try:
			ret.eqSympy = latex2sympy.latex2sympy(latex, latex2sympy.ConfigL2S()).subs(symbols("pi"),sympy.pi) #TODO brauche ich den hiteren .subs
		except Exception as err:
			print("Unable to parse LaTeX code. Either the following code is not valid LaTeX, or there is a bug in latex2sympy: ")
			print(latex)
			print("The exception is:")
			print(err)
			exit(1)
		ret.eqSympy = ret.eqSympy.subs(nameDict)
		ret.eqSympy = ret.eqSympy.subs(nameDict)
		ret.nameDict = nameDict
		return ret
	##Constructs an Equation from a sympy equation, a latex equation, a dictionary that maps latex representations to sympy symbols
	@staticmethod
	def fromSympyAndLatex(sympy, latex, nameDict={}):
		if isinstance(sympy, str):
			sympy = strToSympy(sympy)
		ret = Equation()
		ret.eqSympy = sympy
		ret.eqLatex = latex
		ret.nameDict = nameDict
		return ret
	#! [Equation showHere]
	##\snippet this Equation showHere
	def showHere(self, label=None):
		tex("\\begin{equation}")
		tex(self.eqLatex)
		if label is not None:
			tex("\\label{" + label + "}")
			self.latexLabel = label
		tex("\\end{equation}")
	#! [Equation showHere]
	#@cond INTERNAL
	def __init__(self):
		self.latexLabel = None
	def __repr__(self):
		return "Equation object with: eqSympy=" + str(self.eqSympy) + " eqLatex=" + str(self.eqLatex) + " nameDict=" + str(self.nameDict)
	#@endcond INTERNAL

#@cond INTERNAL
##Check if table contains columns with len(data) == 1. If so, substitute the memName of the column with data[0] in expr
##\param expr: Accepts either a sympy expression or an instance of Equation as an argument.
##\param table: Accepts either a list, a dict or a table as an argument. (Note: The keys of the dictionary don't matter, the memName's do.)
def subsOneValColumn(expr, table):
	if isinstance(table, Table):
		table = list(table.cols.values())
	elif isinstance(table, dict):
		table = list(table.values())
	elif isinstance(table, list):
		pass
	else:
		print("bad argument for subsOneValColumn: table is not of 'Table' or 'dict' or 'list' Type")
		exit(1)
	if isinstance(expr, Equation):
		for c in table:
			if len(c.rawData) == 1:
				expr.eqSympy = expr.eqSympy.subs(c.name.mem, c.data(0))
	else:
		for c in table:
			if len(c.rawData) == 1:
				expr = expr.subs(c.name.mem, c.data(0))
	return expr

def acceptEquation(func, expectedlhs):
	if isinstance(func, str):
		expr = strToSympy(func)
		if isinstance(expr, sympy.Equality):
			assert str(expr.lhs) == expectedlhs.mem, str(expr.lhs) + " != " + expectedlhs.mem
			ret = Equation.fromSympy(expr)
		else:
			ret = Equation.fromSympy(sympy.Equality(symbols(expectedlhs.mem),expr))
	elif isinstance(func, Equation):
		assert str(func.eqSympy.lhs) == expectedlhs.mem, str(func.eqSympy.lhs) + " != " + expectedlhs.mem
		ret = func
	else:
		ret = Equation.fromSympy(sympy.Equality(symbols(expectedlhs.mem),expr))
	return ret
#@endcond INTERNAL