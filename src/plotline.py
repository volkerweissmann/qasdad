#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

#@cond INTERNAl
def listToString(data):
	if data is None:
		return "None"
	ret = "["
	if len(data) != 0:
		ret += str(data[0])
	for i in range(1, len(data)):
		ret += "," + str(data[i])
	ret += "]"
	return ret
#@endcond INTERNAl
##Important Element for class Plot, gets stored in Plot.lines. Consists of physical property that gets shown in the Plot and, optionally, a fitcurve.
class PlotLine: #todo document self.poptSym and self.poptStr
	def __init__(self, logFile, xColumn, yColumn, xerr, yerr, **kwargs):
		self.latexlabel = None
		self.logFile = logFile
		self.fittedfunc = None
		if isinstance(xColumn, np.ndarray):
			self.xcol = Column(Name("numpy"), 1, xColumn)
		elif isinstance(xColumn, list):
			self.xcol = Column(Name("numpy"), 1, np.array(xColumn))
		else:
			self.xcol = Column(xColumn.name, xColumn.basicSIUnit, xColumn.rawData, xColumn.rawDelta)#We need a deep copy of xColumn, xColumn.rawData and xColumn.rawDelta, because we want to remove all nan's from xColumn.rawData
		if isinstance(yColumn, np.ndarray):
			self.ycol = Column(Name("numpy"), 1, yColumn)
		elif isinstance(yColumn, list):
			self.ycol = Column(Name("numpy"), 1, np.array(yColumn))
		else:
			self.ycol = Column(yColumn.name, yColumn.basicSIUnit, yColumn.rawData, yColumn.rawDelta)#Aus mir unerklärlichen Gründen ist das eine deep Copy von yColumn.data, also self.ycol.yColumn[0] = 123 verändern nicht yColumn
		i = 0
		while True and False: #TODO: das hier efficienter machen, evtl. table.deleteFrom benutzen #TODO diese while schleife entweder rausmachen oder aktivieren
			if i >= len(self.xcol.rawData):
				break
			if isnan(self.xcol.rawData[i]) or isnan(self.ycol.rawData[i]):
				self.xcol.rawData = np.delete(self.xcol.rawData, i)
				self.ycol.rawData = np.delete(self.ycol.rawData, i)
				self.xcol.rawDelta = np.delete(self.xcol.rawDelta, i)
				self.ycol.rawDelta = np.delete(self.ycol.rawDelta, i)
			else:
				i += 1
		if xerr:
			self.xdelta = self.xcol.rawDelta
		else:
			self.xdelta = None
		if yerr:
			self.ydelta = self.ycol.rawDelta
		else:
			self.ydelta = None
		if logFile is not None: #We cant just set logFile to /dev/null because listToString is slow for large arrays
			logFile.write("xPlt=" + listToString(self.xcol.rawData)+"\n")
			logFile.write("yPlt=" + listToString(self.ycol.rawData)+"\n")
			logFile.write("xdelta=" + listToString(self.xdelta)+"\n")
			logFile.write("ydelta=" + listToString(self.ydelta)+"\n")
		if "label" in kwargs:
			self.label = kwargs["label"]
		else:
			self.label = "$" + self.ycol.name.getEqName() + "$"
		self.kwargs = kwargs
	##Uses scipy.optimize.curve_fit to fit the function fitfunc to the data in self.xColumn and self.yColumn (if you want to know the origin of self.xColumn and self.yColumn look at the documentation of PlotLine.__init__ and Plot.add).
	##Example usuage (linear Fit for a particle that moves at constant speed)
	##\code{.py}
	#self.fitSym( "position=speed*time + offset" , {speed: meter/second, offset: meter} )
	##\endcode
	##optimal fitparameters get stored in plotLine.poptSym, plotLine.poptStr, plotLine.poptcolSym, plotLine.poptcolStr
	##\param fitfunc Function used to fit. fitfunc shall only contain the fitparameters and xColumn.name.mem as free symbols.
	##\param proposedUnits Sadly, you have to provide a dictionary with the units of every fitparameter. The function checks whether the units are correct. Sadly, only basic SI-Units are allowed.
	##\param p0 Start values for scipy.optimize.curve_fit
	##\param **kwargs All additional arguments are passed to matplotlib.pyplot.plot
	##self.tex_fitfunc will be the latex representation of the fitfunction with the fitparameters as symbols
	##self.tex_fittedfunc will be the latex representation of the fitfunction with the fitparameters as numbers
	def fitSym(self, fitfunc, proposedUnits, p0=None, fakefit=False, xfitlow=None, xfithigh=None, propdelta=False, **kwargs):
		fiteq = acceptEquation(fitfunc, self.ycol.name)
		self.fiteq = fiteq
		self.fitEqLabel = fiteq.latexLabel
		self.tex_fitfunc = fiteq.eqLatex
		#fiteq.eqSympy.rhs self.xcol.name.mem
		proposedUnitsArg = proposedUnits
		startTime = millis()
		if not isinstance(proposedUnitsArg, dict):
			raise TypeError("bad type of proposedUnitsArg: ", type(proposedUnitsArg))
		if p0 is not None and not isinstance(p0, dict):
			raise TypeError("bad type of p0Arg: ", type(p0Arg))
		proposedUnits = {}
		for i in proposedUnitsArg:
			if isinstance(i,str):
				next = symbols(i)
			elif isinstance(i, sympy.symbol.Symbol):
				next = i
			else:
				raise TypeError("bad type in proposedUnitsArg keys:", type(i))
			if subsSIUnitsOne(proposedUnitsArg[i]) != 1:
				raise ValueError("proposedUnits are not SI Units")
			if next in proposedUnits:
				raise ValueError("proposedUnitsArg has conflicts")
			proposedUnits[next] = proposedUnitsArg[i]
		if p0 is not None: #checks if p0 is nice and removes SI units
			p0copy = p0
			p0 = {}
			for i in p0copy:
				if isinstance(i,str):
					next = symbols(i)
				elif isinstance(i, sympy.symbol.Symbol):
					next = i
				else:
					raise TypeError("bad type in p0 keys:", type(i))
				if str(next) == self.xcol.name.mem:
					raise ValueError("You cannot have a fitparameter with the same name as the x-axis.")
				if next in p0:
					raise ValueError("p0 has conflicts")
				if checkGetUnit(p0copy[i]) != proposedUnits[next]:
					raise IncompDimensions("Bad Units: proposedUnitsArg[" + str(next) + "]=" + str(proposedUnits[next]) + " but p0[" + str(next) + "]=" + str(p0copy[i]))
				try:
					p0[next] = float(subsSIUnitsOne(p0copy[i]))
				except:
					raise ValueError("Unable to convert", p0copy[i], " using basicSIUnits:", subsSIUnitsOne(p0copy[i]), " to a float")
		if fakefit:
			for k in proposedUnits:
				if k not in p0:
					raise FreeSymbolException("fakefit is True but p0 is missing the key: " + str(k))
		for s in self.fiteq.eqSympy.rhs.free_symbols:
			if s != symbols(self.xcol.name.mem) and s not in proposedUnits:
				print("bad Arguments for fitX: " + repr(s) + " (of type" + str(type(s)) + ") is an unknown symbol.")
				print("The expression " + str(fiteq.eqSympy.rhs) + " shouldn't contain any symbols except for:")
				print(self.xcol.name.mem)
				for a in proposedUnits:
					print(type(a), repr(a))
				exit(1)
		#if p0 is not None:
		#	for k in p0:
		#		if checkGetUnit(p0[k]) != proposedUnits[k]:
		#			raise IncompDimensions("Bad Units: proposedUnits[" + str(k) + "]=" + proposedUnits[k] + " but p0[" + str(k) + "]=" + str(p0[k]))
		proposedUnits[symbols(self.xcol.name.mem)] = self.xcol.basicSIUnit
		if toBasicSI(checkGetUnit(fiteq.eqSympy.rhs,proposedUnits)) != toBasicSI(self.ycol.basicSIUnit):
			print("wrong dimensions in fitfunc:")
			print(str(self.fiteq.eqSympy.rhs) + " has dimension " + str(toBasicSI(checkGetUnit(self.fiteq.eqSympy.rhs.subs(proposedUnits).subs(symbols(self.xcol.name.mem),self.xcol.basicSIUnit)))))
			print("but ycol.unit is " + str(toBasicSI(self.ycol.basicSIUnit)))
			exit(1)
		all_symbols = [symbols(self.xcol.name.mem)]
		#todo check if self.xcol.name.mem is a fitparameter
		start_pars = []
		for s in fiteq.eqSympy.rhs.free_symbols:
			if s != symbols(self.xcol.name.mem):
				all_symbols.append(s)
				if p0 is not None:
					start_pars.append(p0[s])
		numpy_fitfunc = sympy.lambdify(all_symbols, subsSIUnitsOne(fiteq.eqSympy.rhs), "numpy")
		fitxy = Table()
		fitxy.add(Column(Name("x"), self.xcol.basicSIUnit, self.xcol.rawData, None))
		fitxy.add(Column(Name("y"), self.ycol.basicSIUnit, self.ycol.rawData, None))
		if xfitlow is not None:
			fitxy.removeRows("x < " + str(xfitlow))
			self.xshowfitlow = xfitlow
		else:
			self.xshowfitlow = np.min(fitxy["x"].rawData)
		if xfithigh is not None:
			fitxy.removeRows("x > " + str(xfithigh))
			self.xshowfithigh = xfithigh
		else:
			self.xshowfithigh = np.max(fitxy["x"].rawData)
		
		if p0 is None:
			num = len(fiteq.eqSympy.rhs.free_symbols)-1 #number of data points
			step = len(self.xcol.rawData)//(num+1)
			points = [step*(i+1) for i in range(0,num)]
			popt, pcov = scipy.optimize.curve_fit(numpy_fitfunc, fitxy["x"].rawData[points], fitxy["y"].rawData[points])
			start_pars = popt
		if p0 is None and not fakefit and not propdelta:
			try:
				popt, pcov = scipy.optimize.curve_fit(numpy_fitfunc, fitxy["x"].rawData, fitxy["y"].rawData)
			except Exception as e:
				raise ValueError("scipy.optimize.curve_fit failed, with:\nx=" + repr(self.xcol.rawData) + "\ny=" + repr(self.ycol.rawData) + "\n.Exception=" + str(e))
			sqrtdiag = np.sqrt(np.diag(pcov))
		elif not fakefit and not propdelta:
			try:
				popt, pcov = scipy.optimize.curve_fit(numpy_fitfunc, fitxy["x"].rawData, fitxy["y"].rawData, p0=tuple(start_pars))
			except Exception as e:
				raise ValueError("scipy.optimize.curve_fit failed, with:\np0=" + str(tuple(start_pars)) + "\nx=" + repr(self.xcol.rawData) + "\ny=" + repr(self.ycol.rawData) + "\nnumpy_fitfunc(x,p0)=" + repr(numpy_fitfunc(self.xcol.rawData, *(tuple(start_pars)) ))  + "\n.Exception=" + str(e))
			sqrtdiag = np.sqrt(np.diag(pcov))
		elif p0 is None and not fakefit and propdelta:
			try:
				popt, pcov = scipy.optimize.curve_fit(numpy_fitfunc, fitxy["x"].rawData, fitxy["y"].rawData, sigma=self.ydelta, absolute_sigma=True)
			except Exception as e:
				raise ValueError("scipy.optimize.curve_fit failed, with:\nx=" + repr(self.xcol.rawData) + "\ny=" + repr(self.ycol.rawData) + "\n.Exception=" + str(e))
			sqrtdiag = np.sqrt(np.diag(pcov))
		elif not fakefit and propdelta:
			try:
				popt, pcov = scipy.optimize.curve_fit(numpy_fitfunc, fitxy["x"].rawData, fitxy["y"].rawData, p0=tuple(start_pars), sigma=self.ydelta, absolute_sigma=True)
			except Exception as e:
				raise ValueError("scipy.optimize.curve_fit failed, with:\np0=" + str(tuple(start_pars)) + "\nx=" + repr(self.xcol.rawData) + "\ny=" + repr(self.ycol.rawData) + "\nnumpy_fitfunc(x,p0)=" + repr(numpy_fitfunc(self.xcol.rawData, *(tuple(start_pars)) ))  + "\n.Exception=" + str(e))
			sqrtdiag = np.sqrt(np.diag(pcov))
		else:
			popt = start_pars
			#pcov = np.array([[0]])
			sqrtdiag = np.ones(len(popt))
		for i in range(len(sqrtdiag)):
			if sqrtdiag[i] == np.inf:
				print("Warning: delta is infinity")
				sqrtdiag[i] = popt[i]
		
		self.fittedfunc = lambda x : numpy_fitfunc(x,*popt)
		self.fitkwargs = kwargs
		if "label" in kwargs:
			self.fitlabel = kwargs["label"]
		else:
			self.fitlabel = "Fit"
		
		self.poptSym = {}
		self.poptStr = {}
		self.poptcolSym = {}
		self.poptcolStr = {}
		for i in range(1, len(all_symbols)):
			self.poptSym[all_symbols[i]] = popt[i-1]*proposedUnits[all_symbols[i]]
			self.poptStr[str(all_symbols[i])] = popt[i-1]*proposedUnits[all_symbols[i]]
			
			colname = Name(str(all_symbols[i]))
			inverseEqNameDict = dict([[v,k] for k,v in self.fiteq.nameDict.items()])
			if all_symbols[i] in inverseEqNameDict:
				colname.eqName = inverseEqNameDict[all_symbols[i]]
			
			col = Column(colname, proposedUnits[all_symbols[i]], [popt[i-1]], [sqrtdiag[i-1]])
			self.poptcolSym[all_symbols[i]] = col
			self.poptcolStr[str(all_symbols[i])] = col
			######
		self.tex_fittedfunc = self.tex_fitfunc
		for par in self.poptcolStr:
			#TODO: Das hier funktioniert aus mehreren Gründen: 1. ist par die sympy representation und nicht die latex representation, zweites ist replaceTexSympol nicht perfekt, drittens werden hier unter Umständen nicht benützte Klammern verwendet
			self.tex_fittedfunc = replaceTexSymbol(self.tex_fittedfunc, self.poptcolStr[par].name.getEqName(), "\\left("+self.poptcolStr[par].val(numDigits=0,showDelta=True)+"\\right)")
	##Writes a nice description of the fit using tex(...)
	def describeFit(self):
		if self.latexlabel is None:
			tex("TODO missing latexlabel for describeFit\\\\")
			return
		if getPdfLang() == "en":
			description = "We fitted a function of the form \\begin{equation}" + self.tex_fitfunc + "\\end{equation} "
			if self.fitEqLabel is not None:
				description += "from \\eqref{" + self.fitEqLabel + "} "
			description += "at the values labeled " + self.label + " in figure \\ref{"+self.latexlabel+"}. The fitparameters were determined to be: "
		elif getPdfLang() == "de":
			description = "In Abbildung \\ref{" + self.latexlabel + "} wurde an die in der Legende mit ''" + self.label + "\" bezeichneten Werte mithilfe des Verfahrens der kleinsten Quadrate eine Kurve der Form \\begin{equation}" + self.tex_fitfunc + "\\end{equation} "
			if self.fitEqLabel is not None:
				description += "aus \\eqref{" + self.fitEqLabel + "}"
			description += " angepasst. Die Fitparameter wurden dabei auf folgende Werte bestimmt:"
		description += "\\begin{itemize}"
		arglist = [str(s) for s in self.fiteq.eqSympy.rhs.free_symbols]
		for s in sorted(arglist):
			if s != self.xcol.name.mem:
				description += "\\item $" + s + "=" + self.poptcolStr[s].val(numDigits=0,showDelta=True)  + "$\n" 
		description += "\\end{itemize}"
		tex(description)
		#print(description)
	#@cond INTERNAL
	def updateChash(self, chash):
		chash.update(self.xcol.rawData.data)
		chash.update(self.ycol.rawData.data)
		if self.xdelta is not None:
			chash.update(self.xdelta.data)
		if self.ydelta is not None:
			chash.update(self.ydelta.data)
		chash.update(self.label.encode("UTF-8"))
		updateChash(chash, self.kwargs)
		if self.fittedfunc is not None:
			#chash.update(self.xshowfitlow)
			#chash.update(self.xshowfithigh)
			startX = self.xshowfitlow
			stopX = self.xshowfithigh
			xdata = np.arange(startX,stopX,(stopX-startX)/1000)
			ydata = self.fittedfunc(xdata)
			chash.update(ydata.data)
			updateChash(chash, self.fitkwargs)
	#@endcond INTERNAL