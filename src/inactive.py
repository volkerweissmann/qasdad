#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#This file only contains dead code that is not active but may be activated in further version
#@cond INTERNAL
def getAllLatexSymbols(latex): #TODO talk with the guy who wrote latex2Sympy
	#print(latex)
	splits = [" ", "\t", "\n", "{", "}", "(", ")", "=", "*", "/", "+", "-"]
	allSyms = []
	start = 0
	lastSymPos = 0
	#for i in range(0, len(latex)):
	i = 0
	while i < len(latex):
		#print("########################")
		#print(latex[i:i+5])
		if latex[i] in splits:
			if start != i:
				allSyms.append(latex[start:i])
				lastSymPos = i
			start = i+1
		elif latex[i] == "\\":
			if start != i:
				allSyms.append(latex[start:i])
				lastSymPos = i
			start = i
		elif latex[i] == "_" or latex[i] == "^":
			#print("====================")
			#print(latex[i:i+5])
			lastStart = i 
			#print("lastStart:", latex[lastStart:lastStart+5])
			while latex[lastStart] in splits or latex[lastStart] == "_" or latex[lastStart] == "^" :
				#print("char:", latex[lastStart])
				lastStart -= 1
			if latex[i-1] in splits:
				lastStart -= len(allSyms[-1])-1
			#print("lastStart:", latex[lastStart:lastStart+5])
			if latex[i-1] in splits:
				allSyms.pop()
			#lastStart = lastSymPos
			depth = 0
			quote = False
			alreadyHadSomething = False
			while depth != 0 or not alreadyHadSomething:
				i += 1
				if latex[i] == "\"" or latex[i] == "\'":
					quote = not quote #TODO a_\text{"\" "} würde probleme machen
				elif not quote and latex[i] == "{":
					depth += 1
				elif not quote and latex[i] == "}":
					depth -= 1
				elif not latex[i] in splits:
					alreadyHadSomething = True
			i += 1
			allSyms.append(latex[lastStart:i])
			lastSymPos = i
			start = i
			#print("end of _ or ^")
		i += 1
	if start != len(latex):
		allSyms.append(latex[start:])
	return allSyms





	closeButtonPressed = False
	def handleClose(self, evt):
		self.closeButtonPressed = True
	def interactiveFit(self): #TODO: Document this function and that you (don't) need to specify display=True
		self.closeButtonPressed = False
		matplotlib.pyplot.ion()
		fig = matplotlib.pyplot.figure(0)
		fig.canvas.mpl_connect("close_event", self.handleClose)
		#mng = matplotlib.pyplot.get_current_fig_manager()
		#mng.resize(*mng.window.maxsize())
		matplotlib.pyplot.clf()
		
		ax1 = matplotlib.pyplot.subplot(3,1, 1)
		lineData1, = matplotlib.pyplot.plot(self.xcol.rawData, self.ycol.rawData, color='blue')
		xlow, xhigh = matplotlib.pyplot.xlim()
		ylow, yhigh = matplotlib.pyplot.ylim() 
		ax1.set_xlabel(self.xcol.textName() + " [$" + anythingExceptNumbersToTex(self.xcol.basicSIUnit) + "$]", usetex=True)
		ax1.set_ylabel(self.ycol.textName() + " [$" + anythingExceptNumbersToTex(self.ycol.basicSIUnit) + "$]", usetex=True)
		
		#ax = matplotlib.pyplot.subplot(2,2, 3)
		#matplotlib.pyplot.xlim(xlow, xhigh)
		#matplotlib.pyplot.ylim(ylow, yhigh)
		lineFunc1, = matplotlib.pyplot.plot([], [], color='red')

		ax2 = matplotlib.pyplot.subplot(3,1, 2)
		lineData2, = matplotlib.pyplot.plot(self.xcol.rawData, self.ycol.rawData, color='blue')
		ax2.set_xlabel(self.xcol.textName() + " [$" + anythingExceptNumbersToTex(self.xcol.basicSIUnit) + "$]", usetex=True)
		ax2.set_ylabel(self.ycol.textName() + " [$" + anythingExceptNumbersToTex(self.ycol.basicSIUnit) + "$]", usetex=True)
		lineFunc2, = matplotlib.pyplot.plot([], [], color='red')

		ax3 = matplotlib.pyplot.subplot(3,1, 3)
		ax3.set_xlim(xlow, xhigh)
		ax3.set_xlabel(self.xcol.textName() + " [$" + anythingExceptNumbersToTex(self.xcol.basicSIUnit) + "$]", usetex=True)
		ax3.set_ylabel(self.ycol.textName() + " [$" + anythingExceptNumbersToTex(self.ycol.basicSIUnit) + "$]", usetex=True)
		lineFunc3, = matplotlib.pyplot.plot([], [], color='red')
		
		#axbox = matplotlib.pyplot.axes([0.1, 0.05, 0.8, 0.150])
		#text_box = matplotlib.widgets.TextBox(axbox, self.xcol.name.mem + "=", initial="a*sin(w*" + self.xcol.name.mem + ")   |  a=1*meter  |   w=1/second")
		#text_box.on_submit(lambda text: self.textfieldsubmit(text, ""))
		#matplotlib.pyplot.show(block=False)
		#matplotlib.pyplot.draw()
		#touchfile = open("../interactive.txt", "w")
		#touchfile.write("a*sin(w*" +  self.xcol.name.mem + ")\na=3*" + str(self.ycol.basicSIUnit) + "\nw=7/" + str(self.xcol.basicSIUnit) + "\nxlow=-inf\nxhigh=inf\n")
		#touchfile.write("a+b*" +  self.xcol.name.mem + "\na=0.01\nb=0.3\nxlow=-inf\nxhigh=inf\n")
		#touchfile.close()
		os.system("gedit ../interactive.txt &")
		xlow = -np.inf
		xhigh= np.inf
		while True:
			#if len(lineFunc3.get_xdata() != 0):
			#	ax3.set_xscale("log")
			#else:
			#	ax3.set_xscale("linear")
			#print(lineFunc3.get_xdata())
			matplotlib.pyplot.tight_layout()
			#matplotlib.pyplot.pause(0.1)
			fig.canvas.flush_events()
			time.sleep(0.1)
			if self.closeButtonPressed:
				code = "[\"" + self.label  +"\"]"
				code += ".fitSym(\"" + str(fitfunc).replace("\\","\\\\") + "\", proposedUnits={"
				for p in startpars:
					code += "\"" + str(p)  + "\": 1, " #todo richtige einheiten ausgeben
				code += "}, p0={"
				for p in startpars:
					code += "\"" + str(p)  + "\": " + str(startpars[p]) + ", "
				code += "}"
				code += ", xfitlow=" + str(xlow).replace("-inf", "None").replace("inf", "None")
				code += ", xfithigh=" + str(xhigh).replace("-inf", "None").replace("inf", "None")
				code += ")"
				print(code)
				#break
				exit(1)
			try:
				infile = open("../interactive.txt")
				lines = infile.read().split("\n")
				print(lines, end=" ")
				fitfunc = strToSympy(lines[0])
				startpars = {}
				for tline in lines[1:]:
					ar = tline.split("=")
					if len(ar) == 2:
						lhs = ar[0].lstrip().rstrip()
						if lhs == "xlow":
							xlow = float(ar[1])
						elif lhs == "xhigh":
							xhigh = float(ar[1])
						else:
							startpars[lhs] = strToSympy(ar[1])
				print(startpars, end="\n")
			except Exception as e:
				print(e)
			try:
				lineData1.set_xdata(self.xcol.rawData)
				lineData1.set_ydata(self.ycol.rawData)
				xsel = self.xcol.rawData[(self.xcol.rawData > xlow) & (self.xcol.rawData < xhigh)]
				ysel = self.ycol.rawData[(self.xcol.rawData > xlow) & (self.xcol.rawData < xhigh)]
				xarray = np.arange( np.min(xsel), np.max(xsel), (np.max(xsel)- np.min(xsel))/1000  )
				lineData2.set_xdata(xsel)
				lineData2.set_ydata(ysel)
				#lineData3.set_xdata(xsel)
				#lineData3.set_ydata(ysel)

				#TODO: check the units in fitfunc and startpars
				expr = fitfunc.subs(startpars)
				expr = subsSIUnitsOne(expr)
				print("expr with startpars", expr, end="\n")
				numpy_func = sympy.lambdify(symbols(self.xcol.name.mem), expr, "numpy")
				yar = numpy_func(xarray)
				if isinstance(yar, float):
					yar = np.full((len(xarray)), yar)
				yar = np.array([float(y) for y in yar])
				lineFunc1.set_xdata(xarray)
				lineFunc1.set_ydata(yar)
				lineFunc3.set_xdata(xarray)
				lineFunc3.set_ydata(yar)
				ax3.set_ylim(np.min(yar[(xarray > xlow) & (xarray < xhigh)]), np.max(yar[(xarray > xlow) & (xarray < xhigh)]))
					
				all_symbols = [symbols(self.xcol.name.mem)]
				start_pars = []
				for s in fitfunc.free_symbols:
					if s != symbols(self.xcol.name.mem):
						all_symbols.append(s)
						start_pars.append(startpars[str(s)])
				print("all_symbols:", all_symbols)
				#debugSympyExpr(subsSIUnitsOne(fitfunc))
				#print("subsSIUnitsOne(fitfunc):", subsSIUnitsOne(fitfunc))
				#print("=======================")
				#func = sympy.sympify("V_0 - 2.6214/(p*((k**2 - 1)*sqrt(v**2*exp(-7740.535/T) + 0.25) - 1))")
				#debugSympyExpr(subsSIUnitsOne(fitfunc))
				numpy_fitfunc = sympy.lambdify(all_symbols, subsSIUnitsOne(fitfunc), "numpy")
				try:
					popt, pcov = scipy.optimize.curve_fit(numpy_fitfunc, xsel, ysel)
					lineFunc2.set_xdata(xsel)
					lineFunc2.set_ydata(numpy_fitfunc(xsel, *popt))
					print(popt)
				except:
					lineFunc2.set_xdata([0])
					lineFunc2.set_ydata([0])
				ax1.set_xscale("log")
				ax2.set_xscale("log")
			except Exception as e:
				print(e)
			print("")
		matplotlib.pyplot.ioff()
	def fitTest(self, sympy_fitfunc, subsdict, **kwargs): #TOOD: Document this funktion
		if isinstance(sympy_fitfunc, str):
			sympy_fitfunc = strToSympy(sympy_fitfunc)
		elif isinstance(sympy_fitfunc, Equation):
			if str(sympy_fitfunc.eqSympy.lhs) != self.ycol.name.mem:
				print("bad arguments for fitSym: sympy_fitfunc.lhs != self.ycol.name.mem")
				print(sympy_fitfunc.eqSympy.lhs)
				print(self.ycol.name.mem)
				exit(1)
			sympy_fitfunc = sympy_fitfunc.eqSympy.rhs
		sympy_fitfunc = sympy_fitfunc.subs(subsdict)#TODO einheiten checken
		sympy_fitfunc = sympy_fitfunc.subs(symbols(self.xcol.name.mem), symbols("x"))
		numpy_fitfunc = sympy.lambdify(symbols("x"), subsSIUnitsOne(sympy_fitfunc), "numpy")
		startX = self.xcol.dataForPlot()[0]
		stopX = self.xcol.dataForPlot()[-1]#Performance dataForPlot() gets called multiple times
		xdata = self.xcol.dataForPlot() #TODO das geht doch noch besser
		ydata = numpy_fitfunc(xdata)
		if isinstance(ydata, np.float64):
			#ydata = np.full(len(xdata),self.ycol.withoutUnit()) das war früher hier, aber ich glaube, dass das so falsch ist.
			ydata = np.full(len(xdata),ydata)
		if "label" in kwargs:
			self.ax.plot(xdata, ydata, **kwargs)
		else:
			self.ax.plot(xdata, ydata, label="Test", **kwargs)





#@endcond INTERNAL
