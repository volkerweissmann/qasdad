#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

# def col(*args):
# 	#col(meter, 3.2, 0.1)
# 	if len(args) == 2 and not isinstance(args[0], str) and isinstance(args[1], float):
# 		return Column("unnamed", args[0], np.array([args[1]]))
# 	if len(args) == 3 and not isinstance(args[0], str) and isinstance(args[1], float) and isinstance(args[2], float):
# 		return Column("unnamed", args[0], np.array([args[1]]), rawDelta=np.array([args[2]]))
# 	if len(args) == 3 and isinstance(args[0], str) and isinstance(args[2], float):
# 		return Column(args[0], args[1], np.array([args[2]]))
# 	if len(args) == 3 and isinstance(args[0], str) and isinstance(args[2], np.ndarray):
# 		return Column(args[0], args[1], args[2])
# 	raise NotImplementedError("unknown col")
# def value(value, digits):
# 	return ValueFormat(digits, False).toNiceTexString(value)