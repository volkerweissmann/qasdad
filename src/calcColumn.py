#    QASDAD, the quick and simple data analysis and documentation program
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .qasdad import *

##Calculates a new Column from other Columns using a sympy equation
#Example usuage:
#col = calcColumn(table, "a + sin(b)", "c")
#this would calculate a Column col with the memName "c" and for every i the following equation would be true: col.data(i) = table["a"].data(i) + sin(table["b"].data(i))
#col.analPropUncert is a latex-string that holds the analytic propagation of uncertainty. Show with e.g. tex("$"+col.analPropUncert+"$")
#\param table: Accepts either a list, a dict or a table as an argument. (Note: The keys of the dictionary don't matter, the memName's do.)
#\param equation: The equation used to calculate the new column. Either a string, a sympy expression or an Equation is accepted.
#\return: The calculated Column.
def calcColumn(table, equation, useBetterProp=False):
	if isinstance(table, Table):
		table = table.cols
	elif isinstance(table, dict):
		# for a in table:
		# 	if table[a].name.mem != a:
		# 		raise ValueError("table["+str(a)+"] !="+str(a))
		pass
	elif isinstance(table, list):
		tablelist = table
		table = {}
		for i in tablelist:
			table[i.name.mem] = i
	else:
		raise TypeError("bad argument for calcColumn: table has to of 'Table' or 'dict' or 'list' Type")
	if DEBUG_CHECK_COLS:
		debugCheckColumnTable(table)
		
	if isinstance(equation, Equation):
		eqrhs = equation.eqSympy.rhs
		resultName = Name(str(equation.eqSympy.lhs), eqName=equation.eqLatex.split("=")[0].lstrip().rstrip())
		exampleEqNameDict = equation.nameDict
		exampleEqLatex = equation.eqLatex
	else:
		if isinstance(equation, str):
			equation = strToSympy(equation)
		if isinstance(equation, sympy.Eq):
			resultName = Name(str(equation.lhs), eqName=prettyLatex(equation.lhs)) #See Special 6
			eqrhs = equation.rhs
		else:
			resultName = Name(str(equation), eqName=prettyLatex(equation)) #See Special 6
			eqrhs = equation
		exampleEqNameDict = {}
		for mem in table:
			if mem != table[mem].name.getEqName():
				exampleEqNameDict[table[mem].name.getEqName()] = symbols(mem)
			
		inverseDict = dict([[v,sympy.UnevaluatedExpr(symspace(k))] for k,v in exampleEqNameDict.items()])
		exampleEqLatex = anythingExceptNumbersToTex(equation.subs(inverseDict))
		
	length = 1
	for a in table: #find length of all columns, store it in var length
		if length == 1 and len(table[a].rawData) != 1:
			length = len(table[a].rawData)
		elif length != len(table[a].rawData) and len(table[a].rawData) != 1:
			print("Fatal Error: Lengths don't match")
			exit(1)

	unitdict = {}
	for a in table:
		unitdict[symbols(a)] = table[a].basicSIUnit
	unit = checkGetUnit(eqrhs, unitdict)
	
	ret = Column(resultName, unit, [], rawDelta=[])
	exampleRow = length-1 #Wird in der Beispielrechnugn und für die Fehlerfortpflanzung benötigt

	#exampleCalc = "Beispielrechnung für die Berechnung von $" + ret.name.getEqName() + "$ in Abhängigkeit von:\\begin{itemize}"
	exampleCalcPref = "Beispielhafte Berechnung von $" + ret.name.getEqName() + "$ in Abhängigkeit von:\\begin{itemize}"
	for co in table:
		if sympy.symbols(table[co].name.mem) in eqrhs.free_symbols:
			if len(table[co].rawData) == 1:
				i = 0
			else:
				i = exampleRow
			exampleCalcPref += "\\item $" + table[co].name.getEqName() + "=" + ValueFormat(3,False).toNiceTexString(table[co].data(i))  +"$"
	exampleCalcPref += "\\end{itemize}"
	exampleCalc = exampleEqLatex + "\\\\="
	exampleStep = exampleEqLatex.split("=")[-1]

	all_symbols = []
	for s in eqrhs.free_symbols:
		all_symbols.append(s)
	raw_datas_for_all_symbols = []
	for s in all_symbols:
		raw_datas_for_all_symbols.append(table[str(s)].rawData)
	exampleRowSubs = {}
	realExampleRowSubs = {}
	for s in all_symbols:
		if len(table[str(s)].rawData) == 1:
			exampleRowSubs[s] = table[str(s)].data(0)
			realExampleRowSubs[symbols(str(s), real=True)] = table[str(s)].data(0)
		else:
			exampleRowSubs[s] = table[str(s)].data(exampleRow)
			realExampleRowSubs[symbols(str(s), real=True)] = table[str(s)].data(exampleRow)

	numpy_rhs = sympy.lambdify(all_symbols, subsSIUnitsOne(eqrhs), "numpy")
	ret.rawData = numpy_rhs(*raw_datas_for_all_symbols)
	
	inverseEqNameDict = dict([[v,k] for k,v in exampleEqNameDict.items()])
	eqcp = eqrhs
	for a in eqrhs.free_symbols:
		index = 0
		if len(table[str(a)].rawData) != 1:
			index = exampleRow
		eqcp = eqcp.subs(table[str(a)].name.mem, table[str(a)].data(index))
		if a in inverseEqNameDict:
			exampleStep = replaceTexSymbol(exampleStep, inverseEqNameDict[a], "\\left(" + table[str(a)].val(index, 3, False) + "\\right)")
		else:
			exampleStep = replaceTexSymbol(exampleStep, sympy.latex(str(a)), "\\left(" + table[str(a)].val(index, 3, False) + "\\right)")
	
	realeqrhs = assumeReal(eqrhs)
	diffDict = {} #symbolische Betragsableitungen
	for co in table: #diffDict berechen
		if sympy.symbols(co) in eqrhs.free_symbols and not table[co].isDeltaZero():
			diffDict[co] = np.abs(sympy.diff(realeqrhs,sympy.symbols(table[co].name.mem, real=True))) #TODO wieso steht da np.abs statt sympy.abs?. #real=True is used because of Special 7
	if len(diffDict) == 0:
		ret.rawDelta = None
	else:
		diffDictNumpy = dict([[k,sympy.lambdify(all_symbols, subsSIUnitsOne(v), "numpy")] for k,v in diffDict.items()])
		ret.rawDelta = np.zeros(length)
		for s in all_symbols:
			if not table[str(s)].isDeltaZero():
				if useBetterProp:
					ret.rawDelta += np.power(table[str(s)].rawDelta * diffDictNumpy[str(s)](*raw_datas_for_all_symbols), 2)
				else:
					ret.rawDelta += table[str(s)].rawDelta * diffDictNumpy[str(s)](*raw_datas_for_all_symbols)
		if useBetterProp:
			ret.rawDelta = np.sqrt(ret.rawDelta)
		
	exampleCalc += exampleStep + "\\\\=" + ret.val(exampleRow, 3, False)#+ ValueFormat(0, False).toNiceTexString(ret.data(exampleRow), ret.delta(exampleRow))
	ret.examplePref = exampleCalcPref
	ret.exampleEquation = exampleCalc
	#Ab hier Ist nur noch die analytische Fehlerrechnung
	retFF = ""
	retFF += "\\Delta " + ret.name.getEqName()
	begin = True
	sortedMemNames = sorted([table[co].name.mem for co in table]) #We want a sorted array to make sure that the output qasdad is exactly the same if we run it multiple times
	for co in sortedMemNames: #fügt z.B. = delta x |df/dx| + delta y |df/dy| zu retFF hinzu
		if sympy.symbols(table[co].name.mem) in eqrhs.free_symbols and not table[co].isDeltaZero():
			if begin:
				begin=False
				retFF += "="
				if useBetterProp:
					retFF += "\\sqrt{"
			else:
				retFF += "+"
			if useBetterProp:
				retFF += "\\left(\\Delta " + table[co].name.getEqName() + "\\left|\\frac{\\text d" + ret.name.getEqName()  + "}{\\text d" + table[co].name.getEqName() + "}\\right|\\right)^2"
			else:
				retFF += "\\Delta " + table[co].name.getEqName() + "\\left|\\frac{\\text d" + ret.name.getEqName()  + "}{\\text d" + table[co].name.getEqName() + "}\\right|"
	if useBetterProp:
		retFF += "}"
	retFF += "\\\\"
	memNameMathNameDict = {} #zuordnung name.mem, name.getEqName()
	for co in table:
		if sympy.symbols(table[co].name.mem) in eqrhs.free_symbols:
			memNameMathNameDict[table[co].name.mem] = table[co].name.getEqName()
	begin = True
	retFF += "="
	retFFsymline = "" #z.B. = delta x |sin(x)| + delta y |y^2|
	for co in sortedMemNames: #fügt z.B. = delta x |sin(x)| + delta y |y^2| zu retFF hinzu
		if sympy.symbols(table[co].name.mem) in eqrhs.free_symbols and not table[co].isDeltaZero():
			if begin:
				begin=False
			else:
				retFFsymline += "+"
			#TODO anythinExceptNumbersToTex ist nich so gut, es müssen zahlen auch gerundet werden.
			if useBetterProp:
				retFFsymline += "\\left(\\Delta " + table[co].name.getEqName() + anythingExceptNumbersToTex(diffDict[co].subs(memNameMathNameDict), contextWithspace=True) + "\\right)^2"
			else:
				retFFsymline += "\\Delta " + table[co].name.getEqName() + anythingExceptNumbersToTex(diffDict[co].subs(memNameMathNameDict), contextWithspace=True)
	if useBetterProp:
		retFF += "\\sqrt{" + retFFsymline + "}"
	else:
		retFF += retFFsymline
	retFF += "\\\\"
	countff = 0
	for co in sortedMemNames: #fügt z.B. = 123 + 321 zu retFF hinzu
		if sympy.symbols(table[co].name.mem) in eqrhs.free_symbols and not table[co].isDeltaZero():
			if countff == 0:
				retFF += "="
				if useBetterProp:
					retFF += "\\sqrt{"
			else:
				retFF += "+"
			countff += 1
			if useBetterProp:
				retFF += "\\left("
			if len(table[co].rawDelta) == 1:
				retFF += ValueFormat(4,False).toNiceTexString(table[co].delta(0)*diffDict[co].subs(realExampleRowSubs))
			else:
				retFF += ValueFormat(4,False).toNiceTexString(table[co].delta(exampleRow)*diffDict[co].subs(realExampleRowSubs))
			if useBetterProp:
				retFF += "\\right)^2"
	if useBetterProp:
		retFF += "}"
	if countff > 1:
		retFF += "\\\\="
		retFF += ValueFormat(4,False).toNiceTexString(ret.delta(exampleRow))
	ret.analPropUncert = retFF
	if DEBUG_CHECK_COLS:
		ret.raiseIfBad()
	return ret
#@cond INTERNAL
def calcNfft(tCol,yCol,name, namef): #TODO check wheter tCol has equal steps
	ft = np.fft.fft(yCol.rawData)[:len(yCol.rawData)//2]
	#ft = np.fft.fft(yCol.rawData)
	fty = Column(name, tCol.basicSIUnit*yCol.basicSIUnit, np.abs(ft), rawDelta=None)
	ftx = Column(namef, 1/tCol.basicSIUnit, np.arange(0,len(ft))/np.array(tCol.rawData[-1]), rawDelta=None)
	return [fty, ftx]
#@endcond INTERNAL
