cd ${0%/*} || exit 1
#mkdir qasdad
#echo "from qasdad.imports import *" > qasdad/__init__.py
#cp ../../src/qasdad.py  qasdad/imports.py
#cp ../../src/constants.py qasdad/constants.py
#cp ../../src/units.py qasdad/units.py
#cp ../../src/equation.py qasdad/equation.py
#cp ../../src/table.py qasdad/table.py
#cp ../../src/plot.py qasdad/plot.py
#cp ../../src/main.py qasdad/main.py
#cp ../../src/calcColumn.py qasdad/calcColumn.py

rm -r qasdad
cp -r ../../src qasdad
cp ../../LICENSE.txt LICENSE.txt
cp ../../README.md README.md
python3 setup.py sdist bdist_wheel
