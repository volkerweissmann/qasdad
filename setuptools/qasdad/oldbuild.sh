cd ${0%/*} || exit 1
mkdir qasdad
echo "" > qasdad/__init__.py
cat ../../src/imports.py >> qasdad/__init__.py
cat ../../src/constants.py >> qasdad/__init__.py
cat ../../src/units.py >> qasdad/__init__.py
cat ../../src/equation.py >> qasdad/__init__.py
cat ../../src/table.py >> qasdad/__init__.py
cat ../../src/plot.py >> qasdad/__init__.py
cat ../../src/main.py >> qasdad/__init__.py
cat ../../src/calcColumn.py >> qasdad/__init__.py
cp ../../LICENSE.txt LICENSE.txt
cp ../../README.md README.md
python3 setup.py sdist bdist_wheel
