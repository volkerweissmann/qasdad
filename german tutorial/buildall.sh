cd ${0%/*} || exit 1

build_tut()
{
	cd "$1" || exit 2
	qasdad.py "main.tex"
	if [ $? -ne 0 ]; then
		echo "unable to build following example because qasdad failed."
		echo "$1"
		exit 3
	fi
	cd "main.tex_qasdad_output" || exit 4
	texfot pdflatex main.tex
	if [ $? -ne 0 ]; then
		exit 5
	fi
	texfot pdflatex main.tex
	cd ../.. || exit 6
	
	#cd "$1" || (echo "Unable to build the example: First dir change failed" && exit 1)
	#qasdad.py "main.tex" || ( echo "Unable to build the example: qasdad returned with non zero exit-code" && exit 1)
	#cd "main.tex_qasdad_output" || ( echo "Unable to build the example: Second dir change failed" && exit 1)
	#texfot pdflatex main.tex || ( echo "Unable to build the example: pdflatex failed" && exit 1)
	#cd ../.. || ( echo "Dir change failed" && exit 1 )
}

build_tut "1. Installation und grundlegende Verwendung"
build_tut "2. Einlesen von .dat dateien"
build_tut "3. Diagramme"
build_tut "4. Spalten berechnen"
build_tut "5. Die Equation Klasse"
build_tut "6. Sonstiges"




exit 0

cd "1. Installation und grundlegende Verwendung" || (echo "Unable: build the 1. example: First dir change failed"; exit 1)
qasdad.py "main.tex" || ( echo "Unable: build the 1. example: qasdad returned with non zero exit-code"; exit 1)
cd "main.tex_qasdad_output" || ( echo "Unable: build the 1. example: Second dir change failed"; exit 1)
texfot pdflatex main.tex || ( echo "Unable: build the 1. example: pdflatex failed"; exit 1)
cd ../.. || ( echo "Dir change failed"; exit 1 )

cd "2. Einlesen von .dat dateien" || ( echo "Unable: build the 2. example: First dir change failed"; exit 1)
qasdad.py "main.tex" || ( echo "Unable: build the 2. example: qasdad returned with non zero exit-code"; exit 1)
cd "main.tex_qasdad_output" || ( echo "Unable: build the 2. example: Second dir change failed"; exit 1 )
texfot pdflatex main.tex || ( echo "Unable: build the 2. example: pdflatex failed"; exit 1 )
cd ../.. || ( echo "Dir change failed"; exit 1 )

cd "3. Diagramme" || ( echo "Unable: build the 3. example: First dir change failed"; exit 1 )
qasdad.py "main.tex" || ( echo "Unable: build the 3. example: qasdad returned with non zero exit-code"; exit 1 )
cd "main.tex_qasdad_output" || ( echo "Unable: build the 3. example: Second dir change failed"; exit 1 )
texfot pdflatex main.tex || ( echo "Unable: build the 3. example: pdflatex failed"; exit 1 )
cd ../.. || ( echo "Dir change failed"; exit 1 )

cd "4. Spalten berechnen" || ( echo "Unable: build the 4. example: First dir change failed"; exit 1 )
qasdad.py "main.tex" || ( echo "Unable: build the 4. example: qasdad returned with non zero exit-code"; exit 1 )
cd "main.tex_qasdad_output" || ( echo "Unable: build the 4. example: Second dir change failed"; exit 1 )
texfot pdflatex main.tex || ( echo "Unable: build the 4. example: pdflatex failed"; exit 1 )
cd ../.. || ( echo "Dir change failed"; exit 1 )

cd "5. Die Equation Klasse" || ( echo "Unable: build the 5. example: First dir change failed"; exit 1 )
qasdad.py "main.tex" || ( echo "Unable: build the 5. example: qasdad returned with non zero exit-code"; exit 1 )
cd "main.tex_qasdad_output" || ( echo "Unable: build the 5. example: Second dir change failed"; exit 1 )
texfot pdflatex main.tex || ( echo "Unable: build the 5. example: pdflatex failed"; exit 1 )
cd ../.. || ( echo "Dir change failed"; exit 1 )

cd "6. Sonstiges" || ( echo "Unable: build the 6. example: First dir change failed"; exit 1 )
qasdad.py "main.tex" || ( echo "Unable: build the 6. example: qasdad returned with non zero exit-code"; exit 1 )
cd "main.tex_qasdad_output" || ( echo "Unable: build the 6. example: Second dir change failed"; exit 1 )
texfot pdflatex main.tex || ( echo "Unable: build the 6. example: pdflatex failed"; exit 1 )
cd ../.. || ( echo "Dir change failed"; exit 1 )
